import { RscNewTemplatePage } from './app.po';

describe('RscNew App', function() {
  let page: RscNewTemplatePage;

  beforeEach(() => {
    page = new RscNewTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
