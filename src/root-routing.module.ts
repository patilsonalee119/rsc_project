import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthLayoutComponent } from '@app/layout/auth/auth-layout.component';
// import {HelpComponent} from './app/help/help.component';

const routes: Routes = [
    { path: '', redirectTo: 'account/login', pathMatch: 'full' },

    // {path:'/app/help', component:HelpComponent},

    {
        path: 'account',
        loadChildren: 'account/account.module#AccountModule', //Lazy load account module
        data: { preload: true }
    },
    {
        path: 'app',
        loadChildren: 'app/app.module#AppModule', //Lazy load app module
        data: { preload: true }
    },
    {
        path: '',
        component: AuthLayoutComponent,
        children: [{
            path: 'pages',
            loadChildren: 'app/pages/pages.module#PagesModule' // Lazy load pages module, and hence 'Expedite Component'.
        }]
    }
];

@NgModule({
    declarations: [AuthLayoutComponent], 
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: []
})
export class RootRoutingModule { }