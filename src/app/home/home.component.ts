import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";


@Component({
    templateUrl:'./home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent{
   // @Input() bgColor = 'red';
    constructor(private router:Router){}


    // public getBgColor() {
    //     return {
    //       'background-color': this.bgColor
    //     };
    //   }



Company(){ this.router.navigate(['/app/Company']);}
Site(){ this.router.navigate(['/app/site']);}
CreateUser(){ this.router.navigate(['/app/CreateUser']);}
UserInformation(){ this.router.navigate(['/app/UserInformation']);}
task(){this.router.navigate(['/app/task']);}
subTask(){this.router.navigate(['/app/subTask'])}
Hazards(){this.router.navigate(['./app/hazards'])}
safeguard(){this.router.navigate(['/app/safeguard']);}
pm(){this.router.navigate(['/app/protectivemeasure']);}
safetychain(){this.router.navigate(['/app/safetychain']);}
mainpage(){this.router.navigate(['/app/mainpage']);}
}

