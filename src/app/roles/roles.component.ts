import { Component, Injector, ViewChild } from '@angular/core';
import { MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
    PagedListingComponentBase,
    PagedRequestDto
} from '@shared/paged-listing-component-base';
import {
    RoleServiceProxy,
    RoleDto,
    PagedResultDtoOfRoleDto
} from '@shared/service-proxies/service-proxies';
import { CreateRoleDialogComponent } from './create-role/create-role-dialog.component';
import { EditRoleDialogComponent } from './edit-role/edit-role-dialog.component';

class PagedRolesRequestDto extends PagedRequestDto {
    keyword: string;
}

@Component({
    templateUrl: './roles.component.html',
    animations: [appModuleAnimation()],
    styles: [
        `
          mat-form-field {
            padding: 10px;
          }
        `
    ]
})
export class RolesComponent extends PagedListingComponentBase<RoleDto> {
    roles: RoleDto[] = [];
    dataSourceLength=0
    keyword = '';

    displayedColumns: string[] = ['name','description','actions'];
    dataSource: MatTableDataSource<RoleDto>;

	@ViewChild(MatPaginator,{static: false}) paginator: MatPaginator;
    @ViewChild(MatSort,{static: false}) sort: MatSort;
    loading:boolean=true;

    constructor(
        injector: Injector,
        private _rolesService: RoleServiceProxy,
        private _dialog: MatDialog
    ) {
        super(injector);
    }

    list(
        request: PagedRolesRequestDto,
        pageNumber: number,
        finishedCallback: Function
    ): void {

        request.keyword = this.keyword;

        this._rolesService
            .getAll(request.keyword, request.skipCount, request.maxResultCount)
            .pipe(
                finalize(() => {
                    finishedCallback();
                })
            )
            .subscribe((result: PagedResultDtoOfRoleDto) => {
                this.roles = result.items;
                this.dataSource = new MatTableDataSource(this.roles);
                console.log(this.roles);
                this.loading=false
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;

                this.dataSource.sortingDataAccessor = (data: any, sortHeaderId: string): string => {
                    if (typeof data[sortHeaderId] === 'string') {
                        return data[sortHeaderId].toLocaleLowerCase();
                    }
                    return data[sortHeaderId];
                };
                this.dataSourceLength = this.roles.length;

                this.showPaging(result, pageNumber);
            });
    }
	applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
        this.dataSource.filter = filterValue;
      }
    delete(role: RoleDto): void {
        abp.message.confirm(
            this.l('RoleDeleteWarningMessage', role.displayName),
            (result: boolean) => {
                if (result) {
                    this._rolesService
                        .delete(role.id)
                        .pipe(
                            finalize(() => {
                                abp.notify.success(this.l('SuccessfullyDeleted'));
                                this.refresh();
                            })
                        )
                        .subscribe(() => { });
                }
            }
        );
    }

    createRole(): void {
        this.showCreateOrEditRoleDialog();
    }

    editRole(role: RoleDto): void {
        this.showCreateOrEditRoleDialog(role.id);
    }

    showCreateOrEditRoleDialog(id?: number): void {
        let createOrEditRoleDialog;
        if (id === undefined || id <= 0) {
            createOrEditRoleDialog = this._dialog.open(CreateRoleDialogComponent);
        } else {
            createOrEditRoleDialog = this._dialog.open(EditRoleDialogComponent, {
                data: id
            });
        }

        createOrEditRoleDialog.afterClosed().subscribe(result => {
            if (result) {
                this.refresh();
            }
        });
    }
}
