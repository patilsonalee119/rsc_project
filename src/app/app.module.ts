import { NgModule ,Type} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientJsonpModule } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { ModalModule } from 'ngx-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import {MaterialModule} from './Shared/material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AbpModule } from '@abp/abp.module';

import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { SharedModule } from '@shared/shared.module';

import { HomeComponent } from '@app/home/home.component';
import { AboutComponent } from '@app/about/about.component';
import { TopBarComponent } from '@app/layout/topbar.component';
import { TopBarLanguageSwitchComponent } from '@app/layout/topbar-languageswitch.component';
import { SideBarUserAreaComponent } from '@app/layout/sidebar-user-area.component';
import { SideBarNavComponent } from '@app/layout/sidebar-nav.component';
import { SideBarFooterComponent } from '@app/layout/sidebar-footer.component';
import { RightSideBarComponent } from '@app/layout/right-sidebar.component';
// tenants
import { TenantsComponent } from '@app/tenants/tenants.component';
import { CreateTenantDialogComponent } from './tenants/create-tenant/create-tenant-dialog.component';
import { EditTenantDialogComponent } from './tenants/edit-tenant/edit-tenant-dialog.component';
// roles
import { RolesComponent } from '@app/roles/roles.component';
import { CreateRoleDialogComponent } from './roles/create-role/create-role-dialog.component';
import { EditRoleDialogComponent } from './roles/edit-role/edit-role-dialog.component';
// users
//import { UsersComponent } from '@app/users/users.component';
 //import { ChangePasswordComponent,FormGroupErrorStateMatcher } from './users/change-password/change-password.component';
//import { ResetPasswordDialogComponent } from './users/reset-password/reset-password.component';
import { CompanyComponent } from './company/company.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { AddCompanyComponent } from './company/add-company/add-company.component';

import { SafetychainComponent } from './safetychain/safetychain.component';
import { ToastrModule } from 'ng6-toastr-notifications';
import { AddSiteComponent } from './site/add-site/add-site.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import { RscComponent } from './rsc/rsc.component';
import { MhcComponent } from './mhc/mhc.component';
import { MhscComponent } from './mhsc/mhsc.component';
import { MyCommonModule } from './Shared/common.module';
import { UsersplashComponent } from './usersplash/usersplash.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { AlertService } from './shared/alert.service';
// import { RIAComponent } from './RIA/RIA.component';
import { UsersComponent } from './UerManagement/users.component';
import { CreateUserDialogComponent } from './UerManagement/create-user/create-user-dialog.component';
import { EditUserDialogComponent } from './UerManagement/edit-user/edit-user-dialog.component';




@NgModule({
  declarations: [
    AppComponent,
    // RIAComponent,
    HomeComponent,
    AboutComponent,
    TopBarComponent,
    TopBarLanguageSwitchComponent,
    SideBarUserAreaComponent,
    SideBarNavComponent,
    SideBarFooterComponent,
    RightSideBarComponent,
    TenantsComponent,
    CreateTenantDialogComponent,
    EditTenantDialogComponent,
    RolesComponent,
    CreateRoleDialogComponent,
    EditRoleDialogComponent,
    //UsersComponent,
    CreateUserDialogComponent,
    EditUserDialogComponent,
    //ResetPasswordDialogComponent,
    CompanyComponent,
    CreateUserComponent,
    AddCompanyComponent,
    UsersComponent,
    SafetychainComponent,
    
 
    AddSiteComponent,
    MainpageComponent,
    RscComponent,
    MhcComponent,
    MhscComponent,
    UsersplashComponent
    //UserregisterComponent,
   // ChangepasswordComponent,
    //Register1Component

  ],
  providers: [AlertService,
    { provide: MAT_DIALOG_DATA, useValue: {} },
],
  imports: [
    MyCommonModule,
    CommonModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    ModalModule.forRoot(),
    AbpModule,
    MaterialModule,
    ToastrModule.forRoot(),
    AppRoutingModule,
    ServiceProxyModule,
    SharedModule,
    NgxPaginationModule

  ],
    // { provide: MAT_DIALOG_DATA, useValue: {} }, ],
  entryComponents: [
    CreateTenantDialogComponent,
    EditTenantDialogComponent,
    CreateRoleDialogComponent,
    EditRoleDialogComponent,
    AddCompanyComponent,
    CreateUserDialogComponent,
    EditUserDialogComponent,
    UsersComponent,
    //ResetPasswordDi
    AddSiteComponent,
    // RIAComponent,
    UsersplashComponent

  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]  

})
export class AppModule { }
