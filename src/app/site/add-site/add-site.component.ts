
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { SiteAppServicesServiceProxy } from '@shared/service-proxies/service-proxies';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-site',
  templateUrl: './add-site.component.html',
  styleUrls: ['./add-site.component.css']
})
export class AddSiteComponent implements OnInit {

//   animal:'string';
//   name:'string'
site_name :any
Billing:any

   constructor(public dialog: MatDialog,
   private router:Router,
    private siteservice:SiteAppServicesServiceProxy
    ) { }
//   openDialog():void{
//     const dialogRef = this.dialog.open(AddSiteComponent,{
//       width: '250px',
//       data: {name: this.name, animal: this.animal}
//   });
//   dialogRef.afterClosed().subscribe(result => {
//     console.log('The dialog was closed');
//     this.animal = result;
//   });
// }


savesite(data)
{
  this.siteservice.create(data).subscribe(result=>{
      console.log(data,'addsite')
      this.router.navigate(['./app/Site']);
  })
}

  ngOnInit() {
  }

}






// import { Component, OnInit } from '@angular/core';
// import { SiteAppServicesServiceProxy, Site } from '@shared/service-proxies/service-proxies';

// export interface PeriodicElement {
//   site_name: string;
 
// }

// const ELEMENT_DATA: PeriodicElement[] = [
//    {site_name: 'Hydrogen'},
//    { site_name: 'Helium'},
// ];

// @Component({
//   selector: 'app-add-site',
//   templateUrl: './add-site.component.html',
//   styleUrls: ['./add-site.component.css']
// })
// export class AddSiteComponent implements OnInit {

//   sitelist: Site[] = [];
  
//   displayedColumns: string[] = ['site_name'];
//   dataSource = ELEMENT_DATA;

//   constructor(
//     private siteservice : SiteAppServicesServiceProxy
//   ) { }

//   sitecolumn: string[] = ['site_name'];
//   // dataSource: MatTableDataSource<Site>;


//   ngOnInit() {
//     // this.getSite();
//   }

//   // getSite()
//   // {
//   //   this.siteservice.getAll().subscribe(result=>{
    
//   //     this

//   //   })
//   // }


// }
