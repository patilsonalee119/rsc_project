import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {MatStepperModule} from '@angular/material/stepper';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRouteGuard } from '@shared/auth/auth-route-guard';
import {MaterialModule} from '@app/shared/material.module';
import { ToastrModule } from 'ng6-toastr-notifications';
import { MAT_DIALOG_DATA } from '@angular/material';
import { CommonModule } from '@angular/common';
import { CommonService } from '@app/shared/common.service';
import { SiteComponent } from './site.component';
//import { AddSiteComponent } from './add-site/add-site.component';


const routes : Routes = [
     { path: '', component: SiteComponent},
    // {path:'addsite', component:AddSiteComponent},
  
]

@NgModule({
    declarations:[SiteComponent],
    imports:[
        MatStepperModule,
        FormsModule,
        RouterModule.forChild(routes),
        MaterialModule,
        CommonModule,
        ReactiveFormsModule,
        ToastrModule.forRoot() 
         
    ],
    providers:[ CommonService,
      { provide: MAT_DIALOG_DATA, useValue: {} }, ],
 
 schemas:[CUSTOM_ELEMENTS_SCHEMA]

 
})

export class SiteModule {
}


// import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
// import { Routes, RouterModule } from '@angular/router';

// import { MatStepperModule } from '@angular/material/stepper';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { AppRouteGuard } from '@shared/auth/auth-route-guard';
// import { MaterialModule } from '@app/shared/material.module';
// import { ToastrModule } from 'ng6-toastr-notifications';
// import { MAT_DIALOG_DATA } from '@angular/material';
// import { CommonModule } from '@angular/common';
// // import { CommonService } from '@app/shared/common.service';
// import { AddSiteComponent } from './add-site/add-site.component';
// import { SiteComponent } from './site.component';

// const routes: Routes = [
 
//   { path: '', component: SiteComponent },
//   { path: 'addsite', component: AddSiteComponent },

// ]

// @NgModule({
//   declarations: [
//     AddSiteComponent,
//      SiteComponent
//     ],

//     exports:[
//       AddSiteComponent,
//       SiteComponent],

//   imports: [
//     MatStepperModule,
//     FormsModule,
//     RouterModule.forChild(routes),
//     MaterialModule,
//     ReactiveFormsModule,
//     ToastrModule.forRoot(),
//     CommonModule


//   ],
//   providers: [
//     // CommonService,
//     { provide: MAT_DIALOG_DATA, useValue: {} },],
//   schemas: [CUSTOM_ELEMENTS_SCHEMA]

// })


// export class SiteModule {
// }
