import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SafetychainComponent } from './safetychain.component';

describe('SafetychainComponent', () => {
  let component: SafetychainComponent;
  let fixture: ComponentFixture<SafetychainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SafetychainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SafetychainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
