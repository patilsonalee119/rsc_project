import { Component, OnInit } from '@angular/core';
import {  SafetychainappServiceProxy, FileParameter, Safetychain } from '@shared/service-proxies/service-proxies';
import { ToastrManager } from 'ng6-toastr-notifications';
import { ProtectiveappServiceProxy } from '@shared/service-proxies/service-proxies';
import { SafeguardAppServicesServiceProxy } from '@shared/service-proxies/service-proxies';
import { Router } from "@angular/router";


declare const $: any; // for bootstrap notification

@Component({
  selector: 'app-safetychain',
  templateUrl: './safetychain.component.html',
  styleUrls: ['./safetychain.component.css']
})


export class SafetychainComponent implements OnInit {

  safegaurdlist: any = [];
  protectivelist: any = [];
  public selectedFile: FileParameter;
  SId: any;
  formData: Safetychain;
  pms: any;
  documents: any

  sc_name: any
  safeguard_name:any;
  protective_name:any;

  pm_name: any;
  detectFile(event) {
    this.selectedFile = event.target.files;
  }

  constructor(
    private safegurdservice: SafeguardAppServicesServiceProxy,
    private protectiveservice: ProtectiveappServiceProxy,
    private safetychainservice: SafetychainappServiceProxy,
    private toastr: ToastrManager,
    private router:Router,

  ) { }

  ngOnInit() {
    this.GetAllsafegaurd();
    this.GetAllprotectivelist();
  }

  safeguard: any
  pm: any
  GetAllsafegaurd() {
    this.safegurdservice.getAll().subscribe(result => {

      this.safegaurdlist = result;

      console.log(this.safegaurdlist,"SAfegaurd");


    })
  }

  GetAllprotectivelist() {
    this.protectiveservice.getAll().subscribe(result => {
      this.protectivelist = result;

console.log(this.protectivelist,"PROTECTIVE");

    })
  }


  docfile(SId, file) {

    this.safetychainservice.documentsData(SId, file)
      .subscribe((result) => {
        console.log(result)
      });
  }
  saftgaurds: any;
  savechain(data: any) {
    data.documents = "";
    console.log("Privous Safegauds", data.safeguard_name)
    console.log("Privous PMM", data.protective_name)

    this.saftgaurds = JSON.stringify(data.safeguard_name);
    console.log("after Safegauds", this.saftgaurds)

    this.pms = JSON.stringify(data.protective_name);
    console.log("after PMM", this.pms)

    data.safeguard_name = this.saftgaurds,
      data.protective_name = this.pms;

    this.safetychainservice.create(data).subscribe(result => {
      console.log(data);
      if (this.selectedFile!== undefined) {
        this.SId = result.id;
        const file = { data: this.selectedFile[0], fileName: this.selectedFile[0].name }
        // this.selectedFile.fileName
        this.docfile(this.SId, file);
      }
      let msg = 'Safty chain saved Successfully';
      let type = 'success';
      this.notify(msg, type)
      this.clear()
      //   this.toastr.successToastr('Added Successfully!');
    })
  }
  clear(){

    this.safeguard_name="";
    this.protective_name="";
    this.sc_name="";
  }

  closeDialog()
  {
    this.router.navigate(['/app/home']);
  }
  notify(msg: any, type: any) {
    $.notify({
      icon: "add_alert",
      message: msg
    }, {
        type: type, // type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary']
        placement: {
          from: 'bottom', // from = ['top','bottom']
          align: 'right',
          // align = ['left','center','right']

          animate: {
            enter: 'animated fadeInUp',
            exit: 'animated fadeOutRight'
          },
          offset: 20,
          spacing: 10,
          z_index: 1031,
        }

      });
    setTimeout(() => {

      $.notifyClose();

    }, 2000);

  }
}