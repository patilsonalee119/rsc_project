import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CompanyAppServicesServiceProxy, RSCTaskAppServicesServiceProxy, RSCTask, RSCAppServicesServiceProxy, MHSCAppServicesServiceProxy, MSCAppServicesServiceProxy, RSC } from '@shared/service-proxies/service-proxies';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import 'rxjs/add/operator/finally';
import { Observable } from 'rxjs/Observable';
import { CommonService } from '@app/Shared/common.service';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { TestBed } from '@angular/core/testing';
declare const $: any;


export interface DialogData {
id:any;
}


@Component({
  selector: 'add-task',
  templateUrl: 'addTask.component.html',
  styleUrls: ['addTask.component.css'],
})
export class AddTaskComponent  {

  enableNewTask: boolean;
  tasklist: any = [];
  task_name: any;

  select: any; 
  taskData: any;
  sysData: any; 
  Types: any
 

  RSCTaskedit: RSCTask = new RSCTask();
  buttonShow: boolean = false;

  masterSelected:boolean;
  type:any;
  checkedList:any;

  constructor(
    public dialogRef: MatDialogRef<AddTaskComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private TaskService: RSCTaskAppServicesServiceProxy,
    private router: Router, private dialog: MatDialog,
    private toastr: ToastrManager,
    private rscservice: RSCAppServicesServiceProxy,
    private mhscservice: MHSCAppServicesServiceProxy,
    private mscservice: MSCAppServicesServiceProxy,
    private commonservice: CommonService,
    @Inject(MAT_DIALOG_DATA) private id: number) {

  
      this.type = [
        {value:'RSC'},
        {value:'MSC'},
        {value:'CSC'},
      ];
      this.getCheckedItemList();
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (this.data.id != null) {
      let id= this.data.id
       this.show(id);
    }
  }


  isAllSelected() {
    this.masterSelected = this.type.every(function(item:any) {
        return item.type == true;
      })
    this.getCheckedItemList();
  }
 
  getCheckedItemList(){
    this.checkedList = [];
    for (var i = 0; i < this.type.length; i++) {
      if(this.type[i].isSelected)
      this.type.push(this.type[i]);
    }
    this.checkedList = JSON.stringify(this.checkedList);
  }
 

  show(id: number): void {
    this.TaskService.gettaskForEdit(id).subscribe((result:RSCTask) => {
      this.buttonShow = true;
      this.RSCTaskedit = result;

    })
  }
 

  gettaskdetails() {
    this.TaskService.getAll().subscribe((res) => {
      alert(res);
    })
  }

  saveTask(data: any): void {

   
    if (data.id !== undefined) {
      this.TaskService.update(data)
        .subscribe((result) => {
          this.closeDialog();
        //  console.log(data,'addTaskName')
          let msg = 'Task Updated Successfully';
          let type = 'success';
          this.notify(msg, type)
          this.commonservice.edittaskComponent(true);

        
          this.buttonShow = true;

        })
    }

  

    else {
      data.isActive = 'true';
      this.TaskService.create(data).subscribe((result) => {
        this.closeDialog();
        this.commonservice.callMethodOfRscSTaskComponent(true);
        let msg = ' Task Added Successfully';
        let type = 'success';
        this.notify(msg, type)

         this.buttonShow = true;
      })
     
    }
    //  this.commonservice.callMethodOftaskComponent(true);
  }

closeDialog()
{
  this.dialog.closeAll();
}

  cancelTask()
  {

    this. closeDialog();
   
}
 notify(msg: any, type: any) {
  $.notify({
    icon: "add_alert",
    message: msg
  }, {
      type: type, // type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary']
      placement: {
        from: 'bottom', // from = ['top','bottom']
        align: 'right',
        // align = ['left','center','right']

        animate: {
          enter: 'animated fadeInUp',
          exit: 'animated fadeOutRight'
        },
        offset: 20,
        spacing: 10,
        z_index: 1031,
      }

      });
    setTimeout(() => {
      $.notifyClose();
    }, 2000);
  }




  emailFormArray: Array<any> = [];
  categories = [ 
    {name :"RSC", id: 1},
    {name :"MSC", id: 2},
    {name :"CSC", id: 3},
  ];


 
selectFile(sysName, task_name,event) {

  console.log('taks.....', event.checked);

  this.sysData = task_name
 
  this.taskData = {
    task_name: task_name,
    type: sysName
  }
  this.TaskService.create(this.taskData).subscribe(res => {
    if (sysName == 'RSC' && event.checked == true || sysName == 'MSC' && event.checked == true || sysName == 'CSC' && event.checked == true)
      this.rscservice.create(this.sysData).subscribe(res => {
      })
    // if ()
    //   this.mscservice.create(this.sysData).subscribe(res => {
    //   })
    // if ()
    //   this.mhscservice.create(this.sysData).subscribe(res => {
    //   })
      this.closeDialog();
      let msg = ' added Successfully';
        let type = 'success';
         this.notify(msg, type)
         this.buttonShow = true;  

  })
}



}

