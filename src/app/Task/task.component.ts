import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { AddTaskComponent } from './addTask/addTask.component';
import { RSCTaskAppServicesServiceProxy, RSCTask } from '@shared/service-proxies/service-proxies';
import { ToastrManager } from 'ng6-toastr-notifications';
declare const $: any;
import 'rxjs/add/operator/finally';
import { CommonService } from '@app/Shared/common.service';



@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  loading:boolean;
  router: any; 

  taskColumns: string[] = ['task', 'rsc','msc','csc','defaultcheck','Action'];
  dataSource = new MatTableDataSource<any>();
  dataSourceLength = 0;



  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  _commonservice: any;
  

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  create(){
    const dialogRef = this.dialog.open(AddTaskComponent, {
      width: '550px',
    })
     this.commonservice.callMethodOftaskComponent(false);
  }

  
  constructor(public dialog: MatDialog,
    private toastr: ToastrManager,
    private _taskServiceProxy:RSCTaskAppServicesServiceProxy,
    private commonservice: CommonService) {}

    ngOnInit() {
      this.loading = true ;
      this.GetAllRSCTaskDetails();


      


      this.commonservice.edittaskEvent.subscribe(value => {
      
        if (value=== true) {
          this.GetAllRSCTaskDetails()
       
        }
      })
      this.commonservice.rscstaskEvent.subscribe(value=> {
    
        if(value==true){
  
         this. GetAllRSCTaskDetails()
        }
      })
    }

    GetAllRSCTaskDetails(){
   
      this._taskServiceProxy.getAll().subscribe((result)=>{
      console.log(result); 
      
      this.loading = false ;
      this.dataSourceLength = result.length;

      this.dataSource =new MatTableDataSource(result);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    
      })
    }
    // EditRSCTask(RSCTaskedit): void {
    //   localStorage.setItem('addTask', 'false');
    //   const dialogRef = this.dialog.open(AddTaskComponent, {
    //   data:RSCTaskedit,
    //   });
    //   this._commonservice.editRSCTaskEvent.subscribe(value => {
    //   if (value == true) {
    //   this.GetAllRSCTaskDetails();
    //   }
    // })
    
        
    // }
      


      EditRSCTask(RSCTaskedit): void {
        console.log(RSCTaskedit,"EDITDATA");
        
        const dialogRef = this.dialog.open(AddTaskComponent, {
          data:{id:RSCTaskedit},
          width:'550px'
            }
          
      
        )}
      // this._commonservice.editHazardsEvent.subscribe(value => {
      // if (value == true) {
      // this.GetAllHazardsDetails();
      //   }
      // })
  
    // }

    deleteId:any;
    DeleteTasks(id){
  this.deleteId=id;
    }

    deleteItem(id: any): void {
      console.log(id)
      this._taskServiceProxy.delete(id)
      .finally(() => {
        this.GetAllRSCTaskDetails();
      })
      .subscribe(() => {
       let msg = 'Task Deleted Successfully';
        let type = 'danger';
        this.notify(msg, type) 
    
  
    // deleteItem(id: any): void {
    //   console.log(id)
    //   this._taskServiceProxy.delete(id).subscribe(() => {
    //   this.closeDialog();
    //   let msg = 'task deleted Successfully';
    //   let type = 'danger';
    //   this.notify(msg, type)   
    //   this.GetAllRSCTaskDetails();
      
      })
    }
    closeDialog() {
      this.dialog.closeAll();
    }
    notify(msg: any, type: any) {
      $.notify({
        icon: "add_alert",
        message: msg
      }, {
          type: type, // type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary']
          placement: {
            from: 'bottom', // from = ['top','bottom']
            align: 'right',
            // align = ['left','center','right']
    
            animate: {
              enter: 'animated fadeInUp',
              exit: 'animated fadeOutRight'
            },
            offset: 20,
            spacing: 10,
            z_index: 1031,
          }
    
        });
      setTimeout(() => {
    
        $.notifyClose();
    
      }, 2000);
    }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddTaskComponent, {
      width: '550px',
    
       //color: 'gray',
      // width:'100%',
      // maxWidth: '80vw',
      // height:'500px',
      
      //position: 'static',
     // position: 'static',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      //this.animal = result;
    });

  
  }


  

}
