import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { CompanyAppServicesServiceProxy } from '@shared/service-proxies/service-proxies';
import { Router } from '@angular/router';


export interface DialogData {
  animal: string;
  name: string;
}


@Component({
  selector: 'add-company',
  templateUrl: 'add-company.component.html',
  styleUrls: ['add-company.component.css'],
})
export class AddCompanyComponent  {

  enableNewComapany: boolean;
  companylist:any = [];

  constructor(
    public dialogRef: MatDialogRef<AddCompanyComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private companyservice: CompanyAppServicesServiceProxy,
    private router:Router) {}


  onNoClick(): void {
    this.dialogRef.close();
   
  }

  ngOnInit()
  {
   this.getcompanylist();
  }

  savecompany(data)
  {
    this.companyservice.create(data).subscribe(result=>{
        console.log(data,'addcompanyname')
        this.router.navigate(['./app/addsite']);
    })
  }

  getcompanylist()
  {
    this.companyservice.getAll().subscribe(result=>{
      this.companylist = result;
      console.log(this.companylist,"Clist");
    })
  }

  newcompany(e)
  {
    this.enableNewComapany = e.checked;
    if(e.checked == true)
    {
      
    }
  }


}
