import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatSort, MatPaginator } from '@angular/material';
import { CommonService } from '@app/Shared/common.service';
import { AddHazardsComponent } from './addHazards/addHazards.component';
import {MatTableDataSource} from '@angular/material/table';
import { ToastrManager } from 'ng6-toastr-notifications';
import 'rxjs/add/operator/finally';
import { HazardsAppServicesServiceProxy } from '@shared/service-proxies/service-proxies';
declare const $: any;

@Component({
  selector: 'app-hazards',
  templateUrl: './hazards.component.html',
  styleUrls: ['./hazards.component.css']
})
export class HazardsComponent implements OnInit {

  animal: string;
  name: string; 
  loading:boolean;
  router: any;
  dataSourceLength=0

  hazardsColumns: string[] = ['Hazards', 'RSC','MSC','CSC','Power_Force','Defaultcheck','Action'];
  dataSource = new MatTableDataSource<any>();
  

  
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
 




  create(){
    const dialogRef = this.dialog.open(AddHazardsComponent, {
      width: '500px',
    })
  }
  
  constructor(public dialog: MatDialog,
    private toastr: ToastrManager,
    private commonService: CommonService,
    private _hazardsServiceProxy:HazardsAppServicesServiceProxy,
    
  )  { }

  ngOnInit() {
    this.loading = true ;
    this.GetAllHazardsDetails();

    this.commonService.edithazardsEvent.subscribe(value => {
      
      if (value=== true) {
        this.GetAllHazardsDetails()
     
      }
    })
    this.commonService.rschazardEvent.subscribe(value=> {
    
      if(value==true){

       this. GetAllHazardsDetails()
      }
    })
  }

  GetAllHazardsDetails(){
   
    this._hazardsServiceProxy.getAll().subscribe(result=>{
    console.log(result);   
    this.dataSourceLength= result.length
    
    this.loading = false ;
    this.dataSource =new MatTableDataSource(result);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    
    })
  }
  EditHazards(hazardsedit): void {
    
    const dialogRef = this.dialog.open(AddHazardsComponent, {
    data:hazardsedit,
    width: '550px',
  
    });
    // this._commonservice.editHazardsEvent.subscribe(value => {
    // if (value == true) {
    // this.GetAllHazardsDetails();
    //   }
    // })

  }


  deleteId:any;
  DeleteHazards(id){
this.deleteId=id;
  }

 

  deleteItem(id: any): void {
    console.log(id)
    this._hazardsServiceProxy.delete(id).subscribe(() => {
    this.closeDialog();
    let msg = 'Hazard Deleted Successfully';
    let type = 'danger';
    this.notify(msg, type)   
    this.GetAllHazardsDetails();
  
    })
  }
  closeDialog() {
    this.dialog.closeAll();
  }
  notify(msg: any, type: any) {
    $.notify({
      icon: "add_alert",
      message: msg
    }, {
        type: type, // type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary']
        placement: {
          from: 'bottom', // from = ['top','bottom']
          align: 'right',
          // align = ['left','center','right']
  
          animate: {
            enter: 'animated fadeInUp',
            exit: 'animated fadeOutRight'
          },
          offset: 20,
          spacing: 10,
          z_index: 1031,
        }
  
      });
    setTimeout(() => {
  
      $.notifyClose();
  
    }, 2000);
  }


  openDialog(): void {
    const dialogRef = this.dialog.open(AddHazardsComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }

}

