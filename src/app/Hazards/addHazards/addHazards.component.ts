import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { HazardsAppServicesServiceProxy, Hazards, RSCAppServicesServiceProxy,  MSCAppServicesServiceProxy, CSCAppServicesServiceProxy } from '@shared/service-proxies/service-proxies';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { DialogData } from '@app/company/add-company/add-company.component';
import { CommonService } from '@app/Shared/common.service';

declare const $: any;


@Component({
  selector: 'add-hazards',
  templateUrl: 'addHazards.component.html',
  styleUrls: ['addHazards.component.css'],
})
export class AddHazardsComponent  {

  
  enableNewTask: boolean;
  tasklist:any = [];
  Hazardsedit:Hazards  = new Hazards();
  buttonShow: boolean = false;
  sysData: any;
  select:any;
  hazardsData:any;
  // Power_Force:boolean=false;
  value:any;
  type: any;
  masterSelected: boolean;
  checkedList: any;
  //cscservice: any;

   hazards_name:any
  // sysData: { task: any; };
  // hazData: { haz_name: any; type: any; };
 
  // hazardsData: { 
  //   task_name: any; type: any; };
  
  constructor(
    public dialogRef: MatDialogRef<AddHazardsComponent>,
    private hazardservice: HazardsAppServicesServiceProxy,
    private router:Router, private dialog:MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public toastr: ToastrManager,
    private commonservice: CommonService,
    private rscservice: RSCAppServicesServiceProxy,
    private cscservice: CSCAppServicesServiceProxy,
    private mscservice: MSCAppServicesServiceProxy,
    @Inject(MAT_DIALOG_DATA) private id: number
  
    ) {

      this.type = [
        {value:'RSC'},
        {value:'MSC'},
        {value:'CSC'},
        
      ];
      this.getCheckedItemList();
    }

    
onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit(){
    if (this.id != null) {   
      this.show(this.id);
     }
   
  }

  isAllSelected() {
    this.masterSelected = this.type.every(function(item:any) {
        return item.type == true;
      })
    this.getCheckedItemList();
  }
  getCheckedItemList(){
    this.checkedList = [];
    for (var i = 0; i < this.type.length; i++) {
      if(this.type[i].isSelected)
      this.type.push(this.type[i]);
    }
    
    this.checkedList = JSON.stringify(this.checkedList);
  }
  show(id: number): void {
    this.hazardservice.gethazardsForEdit(id).subscribe((result) => {
      this.buttonShow = true;
      this.Hazardsedit = result;
    })
}
gethazardsdetails()
  {
    this.hazardservice.getAll().subscribe((res)=>{
    alert(res);
    })
  }

saveHazards(data:any):void
  {
    if (data.id !== undefined) {

      this.hazardservice.update(data).subscribe((result) => {
        this.closeDialog();
        this.commonservice.edithazardsComponent(true);
        let msg = 'Hazard Updated Successfully';
        let type = 'success';
        this.notify(msg, type)
        this.buttonShow = true;
       
    })
  
   
  }
    else {
      data.isActive = 'true';
      this.hazardservice.create(data).subscribe((result) => {
       this.closeDialog();
       this.commonservice.callMethodOfRscHazardComponent(true);
        let msg = 'Hazard Added Successfully';
        let type = 'success';
        this.notify(msg, type)
        this.buttonShow = true;
      })
    }
    // this.commonservice.callMethodOfhazardsComponent(true);
  }
  
  



gethazarddetails(){

  this.hazardservice.getAll().subscribe(result=>{
     console.log(result);
  })
  }
  

  cancelHazards()
  {
    this.closeDialog();

  }



closeDialog(){
  this.dialog.closeAll();
}
notify(msg: any, type: any) {
  $.notify({
    icon: "add_alert",
    message: msg
  }, {
      type: type, // type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary']
      placement: {
        from: 'bottom', // from = ['top','bottom']
        align: 'right',
        // align = ['left','center','right']

        animate: {
          enter: 'animated fadeInUp',
          exit: 'animated fadeOutRight'
        },
        offset: 20,
        spacing: 10,
        z_index: 1031,
      }

    });
  setTimeout(() => {

    $.notifyClose();

  }, 2000);
}
emailFormArray: Array<any> = [];
categories = [ 
  {name :"RSC", id: 1},
  {name :"MSC", id: 2},
  {name :"CSC", id: 3},
 
];


 selectFile1(event){
  console.log(this.value);
}
selectFile(sysName, hazards_name,event) {

  console.log('taks.....', event.checked);

  this.sysData = {
    task: hazards_name
  }
  this.hazardsData = {
  task_name: hazards_name,
    type: sysName
  }
  this.hazardservice.create(this.hazardsData).subscribe(res => {
    if (sysName == 'RSC' && event.checked == true)
      this.rscservice.create(this.sysData).subscribe(res => {
      })
    if (sysName == 'MSC' && event.checked == true)
      this.mscservice.create(this.sysData).subscribe(res => {
      })
    if (sysName == 'CSC' && event.checked == true)
      this.cscservice.create(this.sysData).subscribe(res => {
      });

      this.closeDialog();
      let msg = ' added Successfully';
        let type = 'success';
         this.notify(msg, type)
         this.buttonShow = true;  

  })
}

}  

