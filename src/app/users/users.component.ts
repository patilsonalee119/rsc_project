import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { UserServiceProxy, UserDto, PagedResultDtoOfUserDto } from '@shared/service-proxies/service-proxies';
// import { PagedListingComponentBase, PagedRequestDto } from 'shared/paged-listing-component-base';
import { CreateUserComponent } from 'app/users/create-user/create-user.component';
import { EditUserComponent } from 'app/users/edit-user/edit-user.component';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { AlertService } from '@app/shared/alert.service';
import { CommonService } from '@app/shared/common.service';

declare const $: any; // for bootstrap notification

@Component({
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.less'],
    animations: [appModuleAnimation()]
})
export class UsersComponent implements OnInit { //  extends PagedListingComponentBase<UserDto>

    @ViewChild('createUserModal',{static: false}) createUserModal: CreateUserComponent;
    @ViewChild('editUserModal',{static: false}) editUserModal: EditUserComponent;

    loading: boolean;
    dataSourceLength = 0;
    active: boolean = false;
    users: UserDto[] = [];
    displayedColumns: string[] = ['fullName', 'emailAddress', 'isActive', 'actions'];
    dataSource: MatTableDataSource<UserDto>;


    @ViewChild(MatPaginator,{static: false}) paginator: MatPaginator;
    @ViewChild(MatSort,{static: false}) sort: MatSort;

    constructor(
        // injector: Injector,
        private _userService: UserServiceProxy,
        private _alertService: AlertService,
        private _commonService: CommonService
    ) {
        // super(injector);
    }

    ngOnInit() {
        this.list();
        this._commonService.userListDataEvent.subscribe(value => {
            if (value === true) {
                this.list();
            }
        })
        window.scroll(0,0); //scroll starts from top position, at the time of load.
    }

    list(): void {
        this.loading = true;
        this._userService.getAll(0, 500)
        
            .subscribe((result: PagedResultDtoOfUserDto) => {
                this.users = result.items;
                this.dataSourceLength = this.users.length;

                
                this.dataSource = new MatTableDataSource(this.users);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;

                this.dataSource.sortingDataAccessor = (data: any, sortHeaderId: string): string => {
                    if (typeof data[sortHeaderId] === 'string') {
                        return data[sortHeaderId].toLocaleLowerCase();
                    }
                    return data[sortHeaderId];
                };
                this.loading = false;
                // this.showPaging(result, pageNumber);
            });
    }

    //protected list(request: PagedRequestDto, pageNumber: number, finishedCallback: Function): void {
        
    //}

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
        this.dataSource.filter = filterValue;
      }

    protected delete(user: UserDto): void {
        // abp.message.confirm(
        //     'Delete user \'' + user.fullName + '\'?',
        //     (result: boolean) => {
        //         if (result) {
        //             this._userService.delete(user.id)
        //                 .subscribe(() => {
        //                     abp.notify.info('Deleted User: ' + user.fullName);
        //                     this.refresh();
        //                 });
        //         }
        //     }
        // );

        this._alertService.showSwalUser(user.fullName)
        .then((res) => {
            if(res.value == true){
                this._userService.delete(user.id)
                    .finally(() => { 
                        this.list();
                    })
                    .subscribe(() => {
                        let msg= 'Deleted Successfully';
                        let type= 'danger';
                        this.notify(msg,type);
                    })
            }
            // else{
            //     let msg= 'Deletion Cancelled';
            //     let type= 'info';
            //     this.notify(msg,type);
            // }
        })
    }

    // Show Modals
    createUser(): void {
        this.createUserModal.show();
    }

    editUser(user: UserDto): void {
        this.editUserModal.show(user.id);
    }

    notify(msg: any, type: any){
        $.notify({
            icon: "add_alert",
            message: msg 
            
            },{
            type: type, // type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary']
            timer: 4000,
            placement: {
            from: 'bottom', // from = ['top','bottom']
            align: 'right' // align = ['left','center','right']
            }
        });
    }
}
