import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { UserServiceProxy, UserDto, RoleDto } from '@shared/service-proxies/service-proxies';
import { CommonService } from '@app/shared/common.service';
import { AlertService } from '@app/shared/alert.service';
// import { AppComponentBase } from '@shared/app-component-base';

declare const $: any;

@Component({
    selector: 'edit-user-modal',
    templateUrl: './edit-user.component.html'
})
export class EditUserComponent { //  extends AppComponentBase

    @ViewChild('editUserModal',{static: false}) modal: ModalDirective;
    @ViewChild('modalContent',{static: false}) modalContent: ElementRef;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;

    user: UserDto = null;
    roles: any[] = null;
    result1: UserDto;

    constructor(
        // injector: Injector,
        private _userService: UserServiceProxy,
        private _commonService: CommonService,
        private _alertService: AlertService,
    ) {
        // super(injector);
    }

    userInRole(role: RoleDto, user: UserDto): string {
        if (user.roleNames.indexOf(role.normalizedName) !== -1) {
            return "checked";
        }
        else {
            return "";
        }
    }

    show(id: number): void {
        this._userService.getRoles()
            .subscribe((result) => {
                this.roles = result.items;
            });

        this._userService.get(id)
            .subscribe(
            (result) => {
                this.user = result;
                this.active = true;
                this.modal.show();
            }
            );
    }

    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    save(): void {
        var roles = [];
        $(this.modalContent.nativeElement).find("[name=role]").each(function (ind: number, elem: Element) {
            if ($(elem).is(":checked")) {
                roles.push(elem.getAttribute("value").valueOf());
            }
        });
        if (this.user.userName == 'admin') {

            this._alertService.showSwalAdminEdit()
                .then((res) => {

                    // else{
                    //     let msg= 'Deletion Cancelled';
                    //     let type= 'info';
                    //     this.notify(msg,type);
                    // }
                })
        }
        else {

        this.user.roleNames = roles;
        this.user.userName = this.user.emailAddress;

        this.saving = true;
        this._userService.update(this.user)
            .finally(() => { 
                this.saving = false;
                this._commonService.callMethodOfUserComponent(true);/////////////
                console.log(this.result1,"sdfdfsdf");
                
            })
            .subscribe((result) => {
                // this.notify.info(this.l('SavedSuccessfully'));
                this.result1=result;
                let msg= 'Updated Successfully';
                let type= 'success';
                this.notify(msg,type);
                this.close();
                this.modalSave.emit(null);
            });
        }
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    notify(msg: any, type: any){
        $.notify({
            icon: "add_alert",
            message: msg 
            
            },{
            type: type, // type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary']
            timer: 4000,
            placement: {
            from: 'bottom', // from = ['top','bottom']
            align: 'right' // align = ['left','center','right']
            }
        });
    }
}
