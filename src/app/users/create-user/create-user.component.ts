import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { UserServiceProxy, CreateUserDto, RoleDto, EmailSenderServiceProxy } from '@shared/service-proxies/service-proxies';
import { CommonService } from '@app/shared/common.service';
// import { AppComponentBase } from '@shared/app-component-base';

declare const $: any;

@Component({
  selector: 'create-user-modal',
  templateUrl: './create-user.component.html',
  providers: [EmailSenderServiceProxy]
})
export class CreateUserComponent implements OnInit { // extends AppComponentBase 

    @ViewChild('createUserModal',{static: false}) modal: ModalDirective;
    @ViewChild('modalContent',{static: false}) modalContent: ElementRef;

    @Input()selectedIndex: number | null
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    user: CreateUserDto = null;
    roles: RoleDto[] = null;
    path: any;
    Emaildata:any;

    constructor(
        // injector: Injector,
        private _userService: UserServiceProxy,
        private _commonService: CommonService,
        private emailService: EmailSenderServiceProxy
    ) {
        // super(injector);
        this.path = window.location.origin;

    }

    ngOnInit(): void {
        this._userService.getRoles()
        .subscribe((result) => {
            this.roles = result.items;            
        });
    }

    show(): void {
        this.active = true;
        this.modal.show();
        this.user = new CreateUserDto();
        this.user.init({ isActive: true });
    }

    onShown(): void {
        // $.AdminBSB.input.activate($(this.modalContent.nativeElement));
    }

    save(): void {
        //TODO: Refactor this, don't use jQuery style code
        var roles = [];
        $(this.modalContent.nativeElement).find("[name=role]").each((ind:number, elem:Element) => {
            if($(elem).is(":checked") == true){
                roles.push(elem.getAttribute("value").valueOf());
            }
        });

        this.user.roleNames = roles;
        this.user.userName = this.user.emailAddress;

        this.saving = true;
        console.log(this.user);
        if(this.user.password == undefined) {
            this.user.password = '';
        }
        this._userService.create(this.user)
            .finally(() => { 
                this.saving = false; 
                this._commonService.callMethodOfUserComponent(true);
            })
            .subscribe((res) => {
                if(this.user.password == '') {
                    console.log(res);
                    this.sendEmail(res.emailAddress, res.id);
                console.log('email sent');
            }
                let msg= 'Saved Successfully';
                let type= 'success';
                this.notify(msg,type);
                this.close();
                this.modalSave.emit(null);
            });
    }

    sendEmail(mail, uid) {
        const toEmail = mail.toString();
        const emailSub = 'Congrats! Your account created on RealLogix';
        const emailBody = 'Your account has been created on RealLogix Platform.' + 
                        '<br/><a href='+ this.path+ '/pages/userPasswordChange/'+ uid + ' style = "color : red">CLICK HERE</a> to set your new password.';

                        this.Emaildata={
                            toEmail:toEmail,
                            mailSubject:emailSub,
                            mailMessage:emailBody,
                          }               
        this.emailService.sendMail(this.Emaildata).subscribe(result => {
            console.log('Result: ' +result);
        });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    notify(msg: any, type: any){
        $.notify({
            icon: "add_alert",
            message: msg 
            
            },{
            type: type, // type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary']
            timer: 4000,
            placement: {
            from: 'bottom', // from = ['top','bottom']
            align: 'right' // align = ['left','center','right']
            }
        });
    }
}
