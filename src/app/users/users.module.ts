import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AppRouteGuard } from '@shared/auth/auth-route-guard';

import { UsersComponent } from '@app/users/users.component';
import { CreateUserComponent } from '@app/users/create-user/create-user.component';
import { EditUserComponent } from '@app/users/edit-user/edit-user.component';

import { MaterialModule } from '@app/shared/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { ModalModule } from 'ngx-bootstrap';
import { MyCommonModule } from '@app/shared/common.module';
import { CommonService } from '@app/shared/common.service';

const routes: Routes = [
    { path: '', component: UsersComponent, data: { permission: 'Pages.Users' }, canActivate: [AppRouteGuard] },
    { path: '**', redirectTo: 'users' }
]

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        ServiceProxyModule,
        MyCommonModule,
        RouterModule.forChild(routes),
        ModalModule.forRoot(),
    ],
    providers: [
        CommonService
    ],
    declarations: [
        UsersComponent,
        CreateUserComponent,
        EditUserComponent,
    ]
})

export class UsersModule { }
