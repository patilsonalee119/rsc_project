import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.css']
})
export class MainpageComponent implements OnInit {
  //router: any;

  constructor(private router:Router) { }

  rsc(){ this.router.navigate(['/app/rsc']);}
  mhc(){ this.router.navigate(['/app/mhc']);}
  mhsc(){ this.router.navigate(['/app/mhsc']);}

  ngOnInit() {
  }

}
