import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AppRouteGuard } from '@shared/auth/auth-route-guard';

import { UsersComponent } from '@app/users/users.component';
import { CreateUserComponent } from '@app/users/create-user/create-user.component';
import { EditUserComponent } from '@app/users/edit-user/edit-user.component';
import { CommonService } from '@app/Shared/common.service';

import { MaterialModule } from '@app/shared/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { ModalModule } from 'ngx-bootstrap';
import { MyCommonModule } from '@app/shared/common.module';
import { AddSubTaskComponent } from './addSubTask/addSubTask.component';
import { SubTaskComponent } from './subTask.component';


const routes: Routes = [
  { path: '', component: SubTaskComponent, canActivate: [AppRouteGuard] },
  { path: 'add-AddSubTask', component: AddSubTaskComponent },

  { path: '**', redirectTo: '' }
]
@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        ServiceProxyModule,
        MyCommonModule,
        RouterModule.forChild(routes),

        ModalModule.forRoot(),
    ],
    
    declarations: [
      SubTaskComponent,
      AddSubTaskComponent
    ],
    providers:[CommonService]
})

export class subTaskModule { }