import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CompanyAppServicesServiceProxy, RSCsubTaskAppServicesServiceProxy, RSCSubTask, RSCAppServicesServiceProxy, MSCAppServicesServiceProxy, MHSCAppServicesServiceProxy, RSCTaskAppServicesServiceProxy, RscTaskDto } from '@shared/service-proxies/service-proxies';
import { Router } from '@angular/router';
import { CommonService } from '@app/Shared/common.service';
declare const $: any;

export interface DialogData {
  // animal: string;
  // name: string;
  id: any;
}

@Component({
  selector: 'add-AddSubTask',
  templateUrl: 'addSubTask.component.html',
  styleUrls: ['addSubTask.component.css'],
})
export class AddSubTaskComponent {

  enableNewTask: boolean;
  tasklist: any = [];
  subTask_name: any;
  subTaskedit: RSCSubTask = new RSCSubTask();
  buttonShow: boolean = false;
  select: any;
  type: any;

  subtaskData: any;
  sysData: any;

  typelist: string;
  taskObject: any = [];

  taskData: any;
  masterSelected: any;
  checkedList: any;
TaskID: boolean= false;


  constructor(
    public dialogRef: MatDialogRef<AddSubTaskComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private subTaskservice: RSCsubTaskAppServicesServiceProxy,
    private router: Router, private dialog: MatDialog,
    private commonService: CommonService,
    private _TaskService: RSCTaskAppServicesServiceProxy,
    private rscservice: RSCAppServicesServiceProxy,
    private mscservice: MSCAppServicesServiceProxy,
    private mhscservice: MHSCAppServicesServiceProxy,
    @Inject(MAT_DIALOG_DATA) private id: number,

  ) {
  this.type = [
    { value: 'RSC' },
    { value: 'MSC' },
    { value: 'CSC' },
  ];
    this.getCheckedItemList();

  }


  onNoClick(): void {
    this.dialogRef.close();

  }

  ngOnInit() {
    this.showTask();
    // if (this.id != null) {
    //  this.show(this.id);
    // }
    if (this.data.id != null) {
      this.TaskID= true
      let id = this.data.id
      this.show(id);
    }



  }
  isAllSelected() {
    this.masterSelected = this.type.every(function (item: any) {
      return item.type == true;
    })
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.checkedList = [];
    for (var i = 0; i < this.type.length; i++) {
      if (this.type[i].isSelected)
        this.type.push(this.type[i]);
    }
    this.checkedList = JSON.stringify(this.checkedList);
  }
  taskid:any
  show(id: number): void {
    this.subTaskservice.getsubTaskForEdit(id).subscribe((result: RSCSubTask) => {
      this.buttonShow = true;
      this.subTaskedit = result;
      this.taskid = result.taskId;

    })
  }
  Task_ID: any
  Task_name:any
  onChange1(event) {

  console.log(event);
  // alert(event.id)
  
    this.Task_ID = event.id;
    this.Task_name=event.task_name
    
    
  }
  onChange(event){

    this.Task_ID = event;

// alert( this.Task_ID )
this.getTaskName(this.Task_ID)

  }
  getTaskName(id){

    this.subTaskservice.getsubTaskFortASK(id).subscribe((result)=>{

      this.Task_name =result.task_name
      // alert(this.Task_name)
    })
  }
  getsubTaskdetails() {
    this.subTaskservice.getAll().subscribe((res) => {
      alert(res);
    })
  }

  saveSubTask(data: any): void {
    if (data.id !== undefined) {
      data.taskId = this.Task_ID
      data.task_name= this.Task_name

      this.subTaskservice.update(data).
        subscribe((result) => {
          this.closeDialog();
          let msg = 'Subtask Updated Successfully';
          let type = 'success';
          this.notify(msg, type)
      this.commonService.editsubtaskComponent(true);

          this.buttonShow = true;

        })
    }
    else {
      data.isActive = 'true';
 
      console.log(data);
      // alert(this.Task_ID)
      data.taskId = this.Task_ID
      data.task_name= this.Task_name


      this.subTaskservice.create(data)
      .subscribe((result) => {
        console.log(data);

     
      this.closeDialog();
      this.commonService.callMethodOfRscTaskComponent(true);

      let msg = 'Subtask Added Successfully';
      let type = 'success';
      this.notify(msg, type)
      this.buttonShow = true;
    })
    }

    // this.typelist = JSON.stringify(this.safeguard);
    // console.log("after Safegauds", this.saftgaurds)

  }

  cancelsubTask() {

    this.closeDialog();
  }
  closeDialog() {
    this.dialog.closeAll();
  }
  notify(msg: any, type: any) {
    $.notify({
      icon: "add_alert",
      message: msg
    }, {
      type: type, // type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary']
      placement: {
        from: 'bottom', // from = ['top','bottom']
        align: 'right',
        // align = ['left','center','right']

        animate: {
          enter: 'animated fadeInUp',
          exit: 'animated fadeOutRight'
        },
        offset: 20,
        spacing: 10,
        z_index: 1031,
      }

    });
    setTimeout(() => {

      $.notifyClose();

    }, 2000);
  }

  emailFormArray: Array<any> = [];
  categories = [
    { name: "RSC", id: 1 },
    { name: "MSC", id: 2 },
    { name: "CSC", id: 3 },
  ];

  selectFile(sysName, task_name, event) {

    console.log('taks.....', event.checked);

    this.sysData = {
      task: task_name
    }
    this.taskData = {
      task_name: task_name,
      type: sysName
    }
    this.subTaskservice.create(this.taskData).subscribe(res => {
      if (sysName == 'RSC' && event.checked == true)
        this.rscservice.create(this.sysData).subscribe(res => {
        })
      if (sysName == 'MSC' && event.checked == true)
        this.mscservice.create(this.sysData).subscribe(res => {
        })
      if (sysName == 'MHSC' && event.checked == true)
        this.mhscservice.create(this.sysData).subscribe(res => {
        })
      this.closeDialog();
      let msg = ' added Successfully';
      let type = 'success';
      this.notify(msg, type)
      this.buttonShow = true;

    })
  }




  //type = []; RSCChecked; MSCChecked; CSCChecked;
  onCheckboxChange(event, value) {

    if (event.checked) {

      this.type.push(value);
    }

    if (!event.checked) {

      let index = this.type.indexOf(value);
      if (index > -1) {
        this.type.splice(index, 1);
      }
    }

    console.log("Interests array => " + JSON.stringify(this.type, null, 2));
  }

  showTask(): any {
    this._TaskService.getAll().subscribe((result) => {
      this.taskObject = result;
      console.log(result);

    })
  }

  // GetByTask(newValue) {
  //   this._TaskService.(newValue).subscribe(result => {
  //     this.taskObject = result;
  //   })
  // }

}

