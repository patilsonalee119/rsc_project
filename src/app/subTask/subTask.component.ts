import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

import { AddSubTaskComponent } from './addSubTask/addSubTask.component';
import { ToastrManager } from 'ng6-toastr-notifications';
import { RSCsubTaskAppServicesServiceProxy } from '@shared/service-proxies/service-proxies';
declare const $: any;
import 'rxjs/add/operator/finally';
import { CommonService } from '@app/Shared/common.service';


@Component({
  selector: 'app-subTask',
  templateUrl: './subTask.component.html',
  styleUrls: ['./subTask.component.css']
})
export class SubTaskComponent implements OnInit {

  animal: string;
  name: string;
  loading: boolean;
  router: any; 
  dataSourceLength=0;

  subTaskColumns: string[] = ['subTask', 'task', 'rsc', 'msc', 'csc', 'defaultcheck', 'Action'];
  dataSource = new MatTableDataSource<any>();



  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  _commonservice: any;


  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }



  constructor(public dialog: MatDialog,
    private toastr: ToastrManager,
    private _subTaskServiceProxy: RSCsubTaskAppServicesServiceProxy,
    private commonservice: CommonService) { }

  ngOnInit() {
    this.loading = true;
    this.GetAllRSCsubTaskDetails();

    this.commonservice.editsubtaskEvent.subscribe(value => {
      
      if (value=== true) {
        this.GetAllRSCsubTaskDetails()
     
      }
    })
    this.commonservice.rscubtaskEvent.subscribe(value=> {

      if(value==true){

       this. GetAllRSCsubTaskDetails()
      }
    })
  }
  GetAllRSCsubTaskDetails() {

    this._subTaskServiceProxy.getAll().subscribe(result => {
      console.log(result);
      this.dataSourceLength=result.length;
      this.loading = false;
      this.dataSource = new MatTableDataSource(result);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    })
  }


  //     EditRSCsubTask(RSCsubTaskedit): void {
  //       const dialogRef = this.dialog.open(AddSubTaskComponent, {
  //         data: RSCsubTaskedit,


  //     });
  //  }


  EditRSCsubTask(RSCsubTaskedit): void {
    console.log(RSCsubTaskedit, "EDITDATA");

    const dialogRef = this.dialog.open(AddSubTaskComponent, {
      data: { id: RSCsubTaskedit },
      width: '550px'
    }


    )
  }
  //  EditRSCsubTask(RSCsubTaskedit): void {
  //   localStorage.setItem('addTask', 'false');
  //   const dialogRef = this.dialog.open(AddSubTaskComponent, {
  //   data:RSCsubTaskedit,
  //   });
  //   this._commonservice.editRSCTaskEvent.subscribe(value => {
  //   if (value == true) {
  //   this.GetAllRSCsubTaskDetails();
  //     }
  //   })

  // }
  deleteId: any;

  DeleteSubTasks(id) {
    this.deleteId = id;
  }

  deleteItem(id: any): void {
    console.log(id)
    this._subTaskServiceProxy.delete(id)
      .finally(() => {
        this.GetAllRSCsubTaskDetails();
      })
      .subscribe(() => {
        let msg = 'Subtask Deleted Successfully';
        let type = 'danger';
        this.notify(msg, type)

      })
  }
  closeDialog() {
    this.dialog.closeAll();
  }
  notify(msg: any, type: any) {
    $.notify({
      icon: "add_alert",
      message: msg
    }, {
      type: type, // type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary']
      placement: {
        from: 'bottom', // from = ['top','bottom']
        align: 'right',
        // align = ['left','center','right']

        animate: {
          enter: 'animated fadeInUp',
          exit: 'animated fadeOutRight'
        },
        offset: 20,
        spacing: 10,
        z_index: 1031,
      }

    });
    setTimeout(() => {

      $.notifyClose();

    }, 2000);
  }






  openDialog(): void {
    const dialogRef = this.dialog.open(AddSubTaskComponent, {
      width: '550px',

    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }
}

