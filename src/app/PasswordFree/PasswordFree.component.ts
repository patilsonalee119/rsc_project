import { Component, OnInit,Injector, Input } from '@angular/core';
import { CreateUserDto,UserServiceProxy ,RoleDto, EmailSenderServiceProxy } from '@shared/service-proxies/service-proxies';
import { ActivatedRoute, Params } from '@angular/router';
import { AppConsts } from '@shared/AppConsts';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/app-component-base';
import { AbstractControl } from '@angular/forms';
//import { ConfirmEqualValidatorDirective } from '@app/Shared/confirm-equal-validator.directive';
import { Router } from '@angular/router';
declare const $: any;



@Component({
  selector: 'app-PasswordFree',
  templateUrl: './PasswordFree.component.html',
  animations: [appModuleAnimation()]

 
})
export class PasswordFreeComponent  implements OnInit {

  saving: boolean = false;
  //user: CreateUserDto = null;
  user = new CreateUserDto();
  roles: RoleDto[] = null;
  path: any;
  Emaildata: any;
  IsActive: Boolean = true

  emailAddress: any
  name: any
  surname: any;
  password: any;

 UseMail : any =[]
  UserFreeTrail: any;
  confirmSendmail: any;
//  @Input() appConfirmEqualValidator: string;
//  validate(control: AbstractControl): { [key: string]: any } | null {
//      const controlToCompare = control.parent.get(this.appConfirmEqualValidator);
//      if (controlToCompare && controlToCompare.value !== control.value) {
//          return { 'notEqual': true };
//      }

//      return null;
//  }

  constructor(injector: Injector,private router: Router, private Activaterouter: ActivatedRoute,  private _userService: UserServiceProxy,
    private emailSender: EmailSenderServiceProxy) {

    this.Activaterouter.params.subscribe((params: Params) => {
            this.UseMail = params["Encripted"];

           //console.log( "this.UseMail",JSON.parse(this.UseMail.toJSON()));
          
    })
 
   }

  ngOnInit() {
    console.log(this.UseMail,"USERMAIL")
    console.log(atob(this.UseMail),"NEWDATA");
let newTrail =  atob(this.UseMail)
    this.UserFreeTrail = JSON.parse(newTrail);
    console.log( this.UserFreeTrail, "NEWMYKLOCATION")


    this._userService.getRoles()
    .subscribe((result) => {
      this.roles = result.items;
    });



  }

  save(Enitities){
console.log( "AAAA",
Enitities.passwordGroup.password
);

    //var roles = ['Admin'];
console.log( this.UserFreeTrail," this.UserFreeTrail");


    this.user.roleNames = this.UserFreeTrail.roleNames;
    this.user.userName =  this.UserFreeTrail.emailAddress;
    this.user.isActive = true
    this.user.password = Enitities.passwordGroup.password;
    this.user.confirm_password =Enitities.passwordGroup.confirmPassword;
    this.user.totalAmount =this.UserFreeTrail.totalAmount;
    this.user.site=this.UserFreeTrail.site;
    this.user.companyName= this.UserFreeTrail.companyName;
    this.user.name= this.UserFreeTrail.name;
    this.user.surname= this.UserFreeTrail.surname;
    this.user.emailAddress = this.UserFreeTrail.emailAddress


   this.confirmSendmail= Enitities.passwordGroup.confirmPassword



console.log("MYNEWENtity",Enitities)
this._userService.create(this.user)
.subscribe((res) => {

  let msg = 'Saved Successfully';
  let type = 'success';
  this.notify(msg,type);
  this.router.navigateByUrl('/account/login');
});
  // this._userService.create(this.user)
  //     .subscribe((res) => {

  //       let msg = 'Saved Successfully';
  //       let type = 'success';
  //      // this.clear();
  //       this.notify(msg,type);
  //     });
     
      //  var initialUrl =location.href;
      //  // let initialUrl = UrlHelper.initialUrl;
      //   if (initialUrl.indexOf('/account/register') > 0) {
      //       initialUrl = AppConsts.appBaseUrl + '/app/home';
      //   }
      this.sendformail()
      

  }
  
  sendformail() {
    let toEmail;
    let mailSubject;
    let mailMessage;
    
   toEmail= this.UserFreeTrail.emailAddress

  //  akshaymorwadkar@outlook.com

         
     


          mailSubject = "Free Trail EmailID and Password" ;
         
          mailMessage += "EmailID :-" + this.UserFreeTrail.emailAddress;

          mailMessage += "<br>" ;

          mailMessage += "Password :-" + this.confirmSendmail ;




          this.Emaildata = {
            toEmail: toEmail,
            mailSubject: mailSubject,
            mailMessage: mailMessage,
          //  carrierId: carrierID
          }
          this.emailSender.sendMail(this.Emaildata)
            .subscribe(res => {
              // this._carrierService.updateSendMailCount(carrierID).subscribe(res => {
              // })
            });
          // })
        
        
        
        
        

    // });

  }
  notify(msg: any, type: any) {
    $.notify({
      icon: "add_alert",
      message: msg

    }, {
        type: type, // type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary']
        timer: 1000,
        placement: {
          from: 'bottom', // from = ['top','bottom']
          align: 'right' // align = ['left','center','right']
        }
      });
  }
}
