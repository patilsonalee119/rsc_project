import { Component, ViewContainerRef, Injector, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';

import { SignalRAspNetCoreHelper } from '@shared/helpers/SignalRAspNetCoreHelper';

@Component({
    templateUrl: './app.component.html',
  
})
export class AppComponent extends AppComponentBase {
    //implements OnInit, AfterViewInit {

    private viewContainerRef: ViewContainerRef;

    constructor(
        injector: Injector
    ) {
        super(injector);
    }

    ngOnInit(): void {

      
        if(location.href.indexOf('app/home') > 0){
            $('body').attr('class', 'home');
           
            // this.isLoginPage = true;
            // this.isSettingPage = false;
        }
        
        else{
            // $('body').attr('class', 'rscclass');
        }
        
    }


}
