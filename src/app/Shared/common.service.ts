import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class CommonService {

    // private invokeEvent = new Subject <any>();

    private invoketask = new Subject<boolean>();
    private invokesubtask = new Subject<boolean>();
    private invokehazards = new Subject<boolean>();
    private invokesafegaurd = new Subject<boolean>();
    private invokeprotectiveM = new Subject<boolean>();
    private invokesafetychain = new Subject<boolean>();
    private invokeUserListData = new Subject<boolean>();

    private edittask = new Subject<boolean>();
    private editsubtask = new Subject<boolean>();
    private edithazards = new Subject<boolean>();
    private editsafegaurd = new Subject<boolean>();
    private editprotectiveM = new Subject<boolean>();
    private editsafetychain = new Subject<boolean>();
    private rscsubtask = new Subject<boolean>();
    private rsctask = new Subject<boolean>();
    private rschazard = new Subject<boolean>();
    private rscprotective = new Subject<boolean>();
    private rscsafeguard = new Subject<boolean>();
    private ProtectiveListBin= new Subject<boolean>();
private editProtectiveListBin=new Subject<boolean>();

editProtectiveEditforList= this.editProtectiveListBin.asObservable();
    ProtectiveListForAdd= this.ProtectiveListBin.asObservable();
    storeListEvent = this.invoketask.asObservable();
    productListEvent = this.invokesubtask.asObservable();
    rackInfoListEvent = this.invokehazards.asObservable();
    categoryforListEvent = this.invokesafegaurd.asObservable();
    protectiveMListEvent = this.invokeprotectiveM.asObservable();
    safetychainListEvent = this.invokesafetychain.asObservable();
    rscubtaskEvent = this.rscsubtask.asObservable();
    rscstaskEvent = this.rsctask.asObservable();
    rschazardEvent = this.rschazard.asObservable();
    rscprotectiveEvent = this.rscprotective.asObservable();
    rscsafeguardEvent = this.rscsafeguard.asObservable();




    edittaskEvent = this.edittask.asObservable();
    editsubtaskEvent = this.editsubtask.asObservable();
    edithazardsEvent = this.edithazards.asObservable();
    editsafegaurdEvent = this.editsafegaurd.asObservable();
    editprotectiveMEvent = this.editprotectiveM.asObservable();
    editsafetychainEvent = this.editsafetychain.asObservable();

    private commonTypeForm = new Subject<boolean>();
    private editCommonTypeForm = new Subject<number>();

    commonTypeFormEvent = this.commonTypeForm.asObservable();
    editCommonTypeFormEvent = this.editCommonTypeForm.asObservable();
    userListDataEvent = this.invokeUserListData.asObservable();


    constructor() { }

    //recall methods
    callMethodOftaskComponent(number) {
        this.invokeUserListData.next(number);
    }
    callMethodOfUserComponent(number) {
        this.invokeUserListData.next(number);
    }
    callMethodOfstoreComponent(number) {
        this.invoketask.next(number);
    }

    callMethodOfsubtaskComponent(number) {
        this.invokesubtask.next(number);
    }

    callMethodOfhazardInfoComponent(number) {
        this.invokehazards.next(number);
    }
    callMethodOfsafegaurdComponent(number) {
        this.invokesafegaurd.next(number);
    }
    callMethodOfpmInfoComponent(number) {
        this.invokeprotectiveM.next(number);
    }
    callMethodOfsafetychainComponent(number) {
        this.invokesafetychain.next(number);
    }
    callMethodOfRscTaskComponent(number) {
        this.rscsubtask.next(number);
    }
    callMethodOfRscSTaskComponent(number) {
        this.rsctask.next(number);
    }

    callMethodOfRscHazardComponent(number) {
        this.rschazard.next(number);
    }
    callMethodOfRscProtectiveComponent(number) {
        this.rscprotective.next(number);
    }
    callMethodOfRscSafeguardComponent(number) {
        this.rscsafeguard.next(number);
    }

    callMethodOfPMComponent(number) {
        this.ProtectiveListBin.next(number);
    }
    // edit function 

    editProtectiveListBinComponent(id) {
        this.editProtectiveListBin.next(id);
    }

    edittaskComponent(id) {
        this.edittask.next(id);
    }

    editsubtaskComponent(id) {
        this.editsubtask.next(id);
    }

    edithazardsComponent(id) {
        this.edithazards.next(id);
    }
    editsafegaurdsComponent(id) {
        this.editsafegaurd.next(id);
    }
    editprotectiveMComponent(id) {
        this.editprotectiveM.next(id);
    }
    editsafetychainComponent(id) {
        this.editsafetychain.next(id);
    }
}