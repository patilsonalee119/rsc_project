import { ConfirmEqualValidatorDirective } from "./confirm-equal-validator.directive";
import { NgModule } from "@angular/core";


@NgModule({
    declarations: [ConfirmEqualValidatorDirective],
    exports: [ConfirmEqualValidatorDirective]
})

export class MyCommonModule { }
