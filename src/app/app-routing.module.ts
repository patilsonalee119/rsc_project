import { NgModule, Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRouteGuard } from '@shared/auth/auth-route-guard';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { TenantsComponent } from './tenants/tenants.component';
import { RolesComponent } from 'app/roles/roles.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { CompanyComponent } from './company/company.component';
import { AddCompanyComponent } from './company/add-company/add-company.component';
import { AddSiteComponent } from './site/add-site/add-site.component';
import { SafetychainComponent } from './safetychain/safetychain.component';
import { TaskComponent } from './Task/task.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import { RscComponent } from './rsc/rsc.component';
import { MhcComponent } from './mhc/mhc.component';
import { MhscComponent } from './mhsc/mhsc.component';
import { UsersplashComponent } from './usersplash/usersplash.component';
// import {  RIAComponent } from './RIA/RIA.component';
import { UsersComponent } from './UerManagement/users.component';
//import { ChangePasswordComponent } from '@app/ChangePassword/Changepassword.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: AppComponent,
                children: [
                    { path: 'home', component: HomeComponent,  canActivate: [AppRouteGuard] },
                    { path: 'Company', component: CompanyComponent},
                   { path: 'users', component: UsersComponent },
                    { path: 'tenants', component: TenantsComponent, data: { permission: 'Pages.Tenants' }, canActivate: [AppRouteGuard] },
                    { path: 'CreateUser', component: CreateUserComponent},
                 
                    // { path: 'update-password', component: ChangePasswordComponent }
                    {path:'addCompany',component:AddCompanyComponent},
                    {path:'addSite',component:AddSiteComponent},
                    { path: 'safetychain', component: SafetychainComponent},
                    { path: 'mainpage', component:MainpageComponent},
                    { path: 'rsc', component:RscComponent},
                    { path: 'mhc', component:MhcComponent},
                    { path: 'mhsc', component:MhscComponent},
                    { path: 'Usersplash', component:UsersplashComponent},

                    //{ path: 'users', loadChildren: './users/users.module#UsersModule' },
                    { path: 'hazards', loadChildren: './Hazards/hazards.module#hazardsModule' },
                    { path: 'protectivemeasure', loadChildren: './protectivemeasure/protectivemeasure.module#protectivemeasureModule' },

                    { path: 'safeguard', loadChildren: './safeguard/safeguard.module#safeguardModule' },

                    
                    { path: 'subTask', loadChildren: './subTask/subTask.module#subTaskModule' },
                    { path: 'task', loadChildren: './Task/Task.module#TaskModule' },


                    
                    { path: 'Roles', component:RolesComponent},
                    // { path: 'RIA', component:RIAComponent},

                    
                    
                    
                ]
            }
        ])
    ],
 exports: [RouterModule]
})
export class AppRoutingModule { }
