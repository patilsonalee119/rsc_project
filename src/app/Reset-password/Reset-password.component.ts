import { Component, Injector, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { UserServiceProxy, ResetPasswordInput, AccountServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
// import { AbpSessionService } from 'abp-ng2-module/dist/src/session/abp-session.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';





@Component({
    selector: 'app-Reset-password',
    templateUrl: './Reset-password.component.html',
    animations: [appModuleAnimation()]
})


export class ResetPasswordComponent extends AppComponentBase implements OnInit {

    resetpasswordInput: ResetPasswordInput = new ResetPasswordInput();


    UserID: any;
    PasswordCode: any;
    Link :any;
    




        constructor(
            injector: Injector,
            private _userService: AccountServiceProxy,
            // private _sessionService: AbpSessionService,
            private _route: ActivatedRoute,
            private _EmailUserService: AccountServiceProxy,
            private _router: Router

        ) {
            super(injector);
        }

    ngOnInit() {
        this._route.params.subscribe(params => {
            // console.log("in expedite page =>  "+ params['UserID']+ params['PasswordCode']);
       
             this.UserID = params['t1'];
             this.PasswordCode = params['t2'];
        });
    // console.log(this._sessionService.userId);
    window.scroll(0, 0); //scroll starts from top position, at the time of load.
    
    // this._EmailUserService.sendResetID(this.UEmail);

        

  
        
}
   
 
    


save(Enitities): void {


    let mailMessage;

    this.resetpasswordInput.newPassword = Enitities.passwordGroup.password;
    this.resetpasswordInput.userID = this.UserID;
    this.resetpasswordInput.token = this.PasswordCode;


 



        if (this.PasswordCode !=null) {
            // this._EmailUserService.resetPasswordCode(this.resetpasswordInput);
            this._userService.resetPassword(this.resetpasswordInput)
            .subscribe(() => {
                //this.message.info( this.l('Password Changed Successfully')).done(() => {
                //    this._router.navigate(['account/login']);
                //          });
                this._router.navigate(['/account/login']);
                });
           
           }
}
}
