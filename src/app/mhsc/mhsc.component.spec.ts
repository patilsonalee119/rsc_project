import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MhscComponent } from './mhsc.component';

describe('MhscComponent', () => {
  let component: MhscComponent;
  let fixture: ComponentFixture<MhscComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MhscComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MhscComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
