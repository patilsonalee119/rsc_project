import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { SafeguardAppServicesServiceProxy, Safeguard1 } from '@shared/service-proxies/service-proxies';
import { CommonService } from '@app/Shared/common.service';

declare const $: any;
export interface DialogData {
  id:any;
  }

@Component({
  selector: 'app-add-safeguard',
  templateUrl: './add-safeguard.component.html',
  styleUrls: ['./add-safeguard.component.css']
})
export class AddSafeguardComponent implements OnInit {

  enableNewComapany: boolean;
  safeguardlist: any = [];
  safeguard_name: any;
  Safeguardedit: Safeguard1 = new Safeguard1();
  buttonShow: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<AddSafeguardComponent>,
    private safegaurdservice: SafeguardAppServicesServiceProxy,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private router: Router,
    private dialog: MatDialog,
    private commonservice: CommonService,
    @Inject(MAT_DIALOG_DATA) private id: number) { }

  ngOnInit() {
    // if (this.id != null) {   
    // //  this.show(this.id);
    // }
    if (this.data.id != null) {
      let id= this.data.id
       this.show(id);
    }
  }

  show(id: number): void {
      this.safegaurdservice.getsafeguardForEdit(id).subscribe((result:Safeguard1) => {
        this.buttonShow = true;
        this.Safeguardedit = result;
      })
  }

  getsafeguarddetails()
  {
    this.safegaurdservice.getAll().subscribe((res)=>{
    alert(res);
    })
  }

  savesafegaurd(data: any): void {
    if (data.id != undefined) {
    this.safegaurdservice.update(data).subscribe((result) => {
        this.closeDialog();
        let msg = 'Safeguard Updated Successfully';
        let type = 'success';
        this.notify(msg, type)
        this.commonservice.editsafegaurdsComponent(true);
        this.buttonShow = true;
       
      })
      
    }
    else {
      data.isActive = 'true';
      this.safegaurdservice.create(data).subscribe((result) => {
       this.closeDialog();
       this.commonservice.callMethodOfRscSafeguardComponent(true);
        let msg = 'Safeguard Added Successfully';
        let type = 'success';
        this.notify(msg, type)
        this.buttonShow = true;
      })
    }
  }

  cancelsafeguard()
  {
    this.closeDialog();
  }
  

  notify(msg: any, type: any) {
    $.notify({
      icon: "add_alert",
      message: msg
    }, {
        type: type, // type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary']
        placement: {
          from: 'bottom', // from = ['top','bottom']
          align: 'right',
          // align = ['left','center','right']
  
          animate: {
            enter: 'animated fadeInUp',
            exit: 'animated fadeOutRight'
          },
          offset: 20,
          spacing: 10,
          z_index: 1031,
        }
  
      });
    setTimeout(() => {
  
      $.notifyClose();
  
    }, 2000);
  }

   closeDialog(){
     this.dialog.closeAll()
   }

}
