import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSafeguardComponent } from './add-safeguard.component';

describe('AddSafeguardComponent', () => {
  let component: AddSafeguardComponent;
  let fixture: ComponentFixture<AddSafeguardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSafeguardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSafeguardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
