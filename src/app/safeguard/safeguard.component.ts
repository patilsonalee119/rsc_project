import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { AddSafeguardComponent } from './add-safeguard/add-safeguard.component';

import { SafeguardAppServicesServiceProxy } from '@shared/service-proxies/service-proxies';
import 'rxjs/add/operator/finally';
import { CommonService } from '@app/Shared/common.service';

declare const $: any;

@Component({
  selector: 'app-safeguard',
  templateUrl: './safeguard.component.html',
  styleUrls: ['./safeguard.component.css']
})
export class SafeguardComponent implements OnInit {

  animal: string;
  name: string; 
  loading: boolean;
  dataSourceLength=0;

  constructor(public dialog: MatDialog,
    private _safeguardServiceProxy:SafeguardAppServicesServiceProxy,
     private commonservice: CommonService
    ) {}


    ngOnInit() {
      this.loading = true ;
      this.GetAllSafeguardDetails();
      this.commonservice.editsafegaurdEvent.subscribe(value => {
      
        if (value=== true) {
          this.GetAllSafeguardDetails()
       
        }
      })
      this.commonservice.rscsafeguardEvent.subscribe(value=> {
    
        if(value==true){
  
         this. GetAllSafeguardDetails()
        }
      })
    }

  safeguardColumns: string[] = ['safeguard','rsc','msc','csc','defaultcheck','Action'];
  dataSource = new MatTableDataSource<any>();
   
 @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
 @ViewChild(MatSort, {static: true}) sort: MatSort;

 applyFilter(filterValue: string) {
  filterValue = filterValue.trim(); // Remove whitespace
  filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
  this.dataSource.filter = filterValue;
}


  create() {

    const dialogRef = this.dialog.open(AddSafeguardComponent, {
      width: '550px',
    })
}



  GetAllSafeguardDetails(){
   
    this._safeguardServiceProxy.getAll().subscribe((result) => {
      this.loading = false ;
      this.dataSourceLength=result.length;

      this.dataSource =new MatTableDataSource(result);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
   });
 }

 Editsafeguard(Safeguardedit): void {

  const dialogRef = this.dialog.open(AddSafeguardComponent, {
 
  data:{id:Safeguardedit},
  width:'550px'
 
  });
  // this.commonservice.editsafegaurdEvent.subscribe(value => {
  // if (value == true) {
  // this.GetAllSafeguardDetails();
  //   }
  // })
}

deleteId:any;
DeleteSafeguard(id){
this.deleteId=id;
  }

  deleteItem(id: any): void {
    console.log(id)
    this._safeguardServiceProxy.delete(id)
    .finally(() => {
      this.GetAllSafeguardDetails();
 })
    .subscribe(() => {
     let msg = 'Safeguard Deleted Successfully';
      let type = 'danger';
      this.notify(msg, type)  
   
    })
  }


  notify(msg: any, type: any) {
    $.notify({
      icon: "add_alert",
      message: msg
    }, {
        type: type,
        placement: {
          from: 'bottom', 
          align: 'right',
        
          animate: {
            enter: 'animated fadeInUp',
            exit: 'animated fadeOutRight'
          },
          offset: 20,
          spacing: 10,
          z_index: 1031,
        }
  
      });
    setTimeout(() => {
  
      $.notifyClose();
  
    }, 2000);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddSafeguardComponent, {
      width: '550px',
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }
}

// }
