import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { FlexLayoutModule } from '@angular/flex-layout';
import { PagesRoutes } from '@app/pages/pages.routing';

import { SharedModule } from '@shared/shared.module';

//  import { CarrierAwardBidComponent } from '@app/manage-load/add-load/allBids/carrierAwardBid.component
import { ResetPasswordComponent} from '@app/Reset-password/Reset-password.component';
import { HttpModule } from '@angular/http';
import { PasswordFreeComponent } from '@app/PasswordFree/PasswordFree.component';
import { MyCommonModule } from '@app/Shared/common.module';


@NgModule({
  imports: [
    MyCommonModule,
    CommonModule,
    HttpModule,
    RouterModule.forChild(PagesRoutes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [
 
     ResetPasswordComponent,
     PasswordFreeComponent
 
  ]
})

export class PagesModule {}
