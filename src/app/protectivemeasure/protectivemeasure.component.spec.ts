import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProtectivemeasureComponent } from './protectivemeasure.component';

describe('ProtectivemeasureComponent', () => {
  let component: ProtectivemeasureComponent;
  let fixture: ComponentFixture<ProtectivemeasureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProtectivemeasureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProtectivemeasureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
