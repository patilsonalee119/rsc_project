import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { AddProtectiveMeasureComponent } from './add-protective-measure/add-protective-measure.component';
import { ProtectiveappServiceProxy } from '@shared/service-proxies/service-proxies';
import { CommonService } from '@app/Shared/common.service';

declare const $: any;
@Component({
  selector: 'app-protectivemeasure',
  templateUrl: './protectivemeasure.component.html',
  styleUrls: ['./protectivemeasure.component.css']
})
export class ProtectivemeasureComponent implements OnInit {

  pmColumns: string[] = ['protective_measure', 'RSC', 'MSC', 'CSC', 'Default', 'Action'];
  dataSource = new MatTableDataSource<any>();
  loading: boolean;
  dataSourceLength=0;

  constructor(public dialog: MatDialog,
    private _CommonService:CommonService,
    private pmservice: ProtectiveappServiceProxy) { }
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;
  
    applyFilter(filterValue: string) {
      filterValue = filterValue.trim(); // Remove whitespace
      filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
      this.dataSource.filter = filterValue;
    }

  ngOnInit() {
    this.loading = true ;
    this.GetAllpmDetails();
    this._CommonService.ProtectiveListForAdd.subscribe((value)=>{

    if (value== true){

      this.GetAllpmDetails();
    }
    })
    this._CommonService.editProtectiveEditforList.subscribe((result)=>{

      if(result==true){

        this.GetAllpmDetails();

      }
    })
  }
  GetAllpmDetails() {

    this.pmservice.getAll().subscribe(result => {
      console.log(result);
      this.loading = false;
      this.dataSourceLength=result.length;

      this.dataSource = new MatTableDataSource(result);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    })
  }


  EditProtective(pmedit) {
    const dialogRef = this.dialog.open(AddProtectiveMeasureComponent, {
      // data: pmedit,
      data:{id:pmedit},
      width: '500px',
    });
  }
  notify(msg: any, type: any) {
    $.notify({
      icon: "add_alert",
      message: msg
    }, {
        type: type,
        placement: {
          from: 'bottom',
          align: 'right',
           animate: {
            enter: 'animated fadeInUp',
            exit: 'animated fadeOutRight'
          },
          offset: 20,
          spacing: 10,
          z_index: 1031,
        }
  
      });
    setTimeout(() => {
  
      $.notifyClose();
  
    }, 2000);
  }
  deleteId: any;
  DeleteProtective(id) {
    this.deleteId = id;
  }

  deleteItem(id: any): void {
    console.log(id)
    this.pmservice.delete(id).subscribe(() => {
    this.closeDialog();
    let msg = 'Protective Measure Deleted Successfully';
    let type = 'danger';
    this.notify(msg, type)   
    this.GetAllpmDetails();
  
    })
  }
  closeDialog() {
    this.dialog.closeAll();
  }
 openDialog(): void {
    const dialogRef = this.dialog.open(AddProtectiveMeasureComponent, {
      width: '500px',
     
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    
    });
  }

}
