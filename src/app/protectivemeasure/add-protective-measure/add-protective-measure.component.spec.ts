import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddProtectiveMeasureComponent } from './add-protective-measure.component';

describe('AddProtectiveMeasureComponent', () => {
  let component: AddProtectiveMeasureComponent;
  let fixture: ComponentFixture<AddProtectiveMeasureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddProtectiveMeasureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddProtectiveMeasureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
