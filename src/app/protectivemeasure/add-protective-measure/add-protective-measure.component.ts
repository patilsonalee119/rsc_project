import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ProtectiveappServiceProxy, Protective, RSCAppServicesServiceProxy, MSCAppServicesServiceProxy, CSCAppServicesServiceProxy } from '@shared/service-proxies/service-proxies';
import { Router } from '@angular/router';
import { ToastrModule, ToastrManager } from 'ng6-toastr-notifications';
import { data } from 'jquery';
import { CommonService } from '@app/Shared/common.service';

declare const $: any;
export interface DialogData {
  id:any;
  }

@Component({
  selector: 'app-add-protective-measure',
  templateUrl: './add-protective-measure.component.html',
  styleUrls: ['./add-protective-measure.component.css']
})

export class AddProtectiveMeasureComponent implements OnInit {

  enableNewTask: boolean;
  tasklist:any = [];
  Protectiveedit:Protective  = new Protective();
  buttonShow: boolean = false;
  sysData: any;
  select:any;
  ProtectiveData:any;
  // Power_Force:boolean=false;
  value:any;
  type: any;
  masterSelected: boolean;
  checkedList: any;

  protective_name:any;
 
  
  pmData: { task_name: any; type: any; };  
  

  constructor(
    public dialogRef: MatDialogRef<AddProtectiveMeasureComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public toastr: ToastrManager,
    private pmservice: ProtectiveappServiceProxy,
    private rscservice:RSCAppServicesServiceProxy,
    private mscservice:MSCAppServicesServiceProxy,
    private cscservice:CSCAppServicesServiceProxy,
   private _CommonService:CommonService,
    private router:Router,
    private dialog:MatDialog,
    @Inject(MAT_DIALOG_DATA) private id: number) {
      this.type = [
        {value:'RSC'},
        {value:'MSC'},
        {value:'CSC'},
        
      ];
      this.getCheckedItemList();
    }

  
    
 ngOnInit() {
  if (this.data.id != null) {
    let id= this.data.id
     this.show(id);
  }
  }

  isAllSelected() {
    this.masterSelected = this.type.every(function(item:any) {
        return item.type == true;
      })
    this.getCheckedItemList();
  }
  getCheckedItemList(){
    this.checkedList = [];
    for (var i = 0; i < this.type.length; i++) {
      if(this.type[i].isSelected)
      this.type.push(this.type[i]);
    }
    
    this.checkedList = JSON.stringify(this.checkedList);
  }
  show(id: number): void {
    this.pmservice.getpmForEdit(id).subscribe((result) => {
      this.buttonShow = true;
      this.Protectiveedit = result;
    })
}
getprotective(){

  this.pmservice.getAll().subscribe((result)=>{

console.log(result,"PM");

          
  })
}
getpmdetails()
  {
    this.pmservice.getAll().subscribe((res)=>{
    alert(res);
    })
  }
  saveprotective(data:any):void
  {
    if (data.id !== undefined) {

      this.pmservice.update(data).subscribe((result) => {
        this.closeDialog();
        let msg = 'Protective Measure Updated Successfully';
        let type = 'success';
        this._CommonService.editProtectiveListBinComponent(true);
        // this.getprotective()
        this.notify(msg, type)
        this.buttonShow = true;
    
    })
  
   
  }
    else {
      data.isActive = 'true';
      this.pmservice.create(data).subscribe((result) => {
       this.closeDialog();
       this._CommonService.callMethodOfPMComponent(true);
        let msg = 'Protective Measure Added Successfully';
        let type = 'success';
        this.notify(msg, type)
        this.buttonShow = true;
      })
    }
    // this.commonservice.callMethodOfhazardsComponent(true);
  }
  // saveprotective(data)
  // {
  //   this.pmservice.create(data).subscribe(result=>{
  //       console.log(data,'pm_name')
  //       let msg = 'PM saved Successfully';
  //       let type = 'success';
  //       this._CommonService.callMethodOfPMComponent(true);
  //       this.notify(msg, type)
  //       this.closeDialog();
  //   })
  // }

  getprotectivedetails(){

    this.pmservice.getAll().subscribe(result=>{
       console.log(result);
    })
    }
    
  
    cancelProtective()
    {
      this.closeDialog();
  
    }
    closeDialog(){
      this.dialog.closeAll();
    }
  notify(msg: any, type: any) {
    $.notify({
      icon: "add_alert",
      message: msg
    }, {
        type: type, // type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary']
        placement: {
          from: 'bottom', // from = ['top','bottom']
          align: 'right',
          // align = ['left','center','right']
  
          animate: {
            enter: 'animated fadeInUp',
            exit: 'animated fadeOutRight'
          },
          offset: 20,
          spacing: 10,
          z_index: 1031,
        }
  
      });
    setTimeout(() => {
  
      $.notifyClose();
  
    }, 2000);
}
emailFormArray: Array<any> = [];
categories = [ 
  {name :"RSC", id: 1},
  {name :"MSC", id: 2},
  {name :"CSC", id: 3},
 
];


 selectFile1(event){
  console.log(this.value);
}
selectFile(sysName, protective_name,event) {

  console.log('taks.....', event.checked);

  this.sysData = {
    task: protective_name
  }
  this.pmData = {
  task_name: protective_name,
    type: sysName
  }
  this.pmservice.create(this.ProtectiveData).subscribe(res => {
    if (sysName == 'RSC' && event.checked == true)
      this.rscservice.create(this.sysData).subscribe(res => {
      })
    if (sysName == 'MSC' && event.checked == true)
      this.mscservice.create(this.sysData).subscribe(res => {
      })
    if (sysName == 'CSC' && event.checked == true)
      this.cscservice.create(this.sysData).subscribe(res => {
      });

      this.closeDialog();
      let msg = ' added Successfully';
        let type = 'success';
         this.notify(msg, type)
         this.buttonShow = true;  

  })
}
}
