import { Component, OnInit } from '@angular/core';
import { CompanyAppServicesServiceProxy } from '@shared/service-proxies/service-proxies';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  companylist:any = [];
  company_name:any;
  constructor(
    private companyservice: CompanyAppServicesServiceProxy,
    private router:Router
  ) { }

  ngOnInit() {
    this.getcompanylist();
  }


  getcompanylist()
  {
    this.companyservice.getAll().subscribe(result=>{
      this.companylist = result;
      console.log(this.companylist,"Clist");
    })
  }
}
