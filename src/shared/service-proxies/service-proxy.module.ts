import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AbpHttpInterceptor } from '@abp/abpHttpInterceptor';

import * as ApiServiceProxies from './service-proxies';


@NgModule({
    providers: [
        ApiServiceProxies.RoleServiceProxy,
        ApiServiceProxies.SessionServiceProxy,
        ApiServiceProxies.TenantServiceProxy,
        ApiServiceProxies.UserServiceProxy,
        ApiServiceProxies.TokenAuthServiceProxy,
        ApiServiceProxies.AccountServiceProxy,
        ApiServiceProxies.ConfigurationServiceProxy,
        ApiServiceProxies.CompanyAppServicesServiceProxy,
        ApiServiceProxies.SafeguardAppServicesServiceProxy,
        ApiServiceProxies.SiteAppServicesServiceProxy,
        ApiServiceProxies.RSCTaskAppServicesServiceProxy,
        ApiServiceProxies.RSCsubTaskAppServicesServiceProxy,
        ApiServiceProxies.HazardsAppServicesServiceProxy,
        ApiServiceProxies.ProtectiveappServiceProxy,
        ApiServiceProxies.SafetychainappServiceProxy,
        ApiServiceProxies.EmailSenderServiceProxy,

        ApiServiceProxies.RSCAppServicesServiceProxy,
        ApiServiceProxies.MHSCAppServicesServiceProxy,
        ApiServiceProxies.MSCAppServicesServiceProxy,
        ApiServiceProxies.AccountServiceProxy,
        ApiServiceProxies.EmailSenderServiceProxy,
        ApiServiceProxies.CSCAppServicesServiceProxy,

        { provide: HTTP_INTERCEPTORS, useClass: AbpHttpInterceptor, multi: true }
    ]
})
export class ServiceProxyModule { }
