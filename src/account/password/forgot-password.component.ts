import { Component, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/app-component-base';
import { AccountServiceProxy, SendPasswordResetCodeInput} from '@shared/service-proxies/service-proxies';
import { AppUrlService } from '@shared/nav/app-url.service';
import { accountModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    templateUrl: './forgot-password.component.html',
    animations: [accountModuleAnimation()]
})
export class ForgotPasswordComponent extends AppComponentBase {

    model: SendPasswordResetCodeInput = new SendPasswordResetCodeInput();


    saving = false;
    emailAddress: any;

    constructor(
        injector: Injector,
        private _accountService: AccountServiceProxy,
        private emailSender: AccountServiceProxy,
        private _appUrlService: AppUrlService,
        private _router: Router
    ) {
        super(injector);
    }

    save( entity): void
    {

        
    let mailMessage;
 
        //    this.saving = true;
        //    this._accountService.sendPasswordResetCode(this.model)
        //        .finally(() => { this.saving = false; })
        //        .subscribe(() => {
        //            this.message.success(this.l('PasswordResetMailSentMessage'), this.l('MailSent')).done(() => {
        //                this._router.navigate(['account/login']);
        //            });
        //        });




        // let useremail = this.emailAddress;
        // let toEmail;
        // let mailSubject;
        // let mailMessage;

        // let emailAddresses: any[] = [];

        // emailAddresses.push(useremail);
        // toEmail = useremail;
        // mailSubject = "Forgot Password";
        // mailMessage = "Hello sir , ";
        // mailMessage += "<br /> <br />"

        // mailMessage += "<br /> <br />"
        mailMessage = window.location.origin;

        // **here time taken as creationTime for understanding, actually it should be lastModificationTime.



        this.saving = true;
        this.emailSender.sendPasswordResetCode(entity,mailMessage)
        
        
            .subscribe(() => {
                //this.message.confirm(this.l('Email has been sent to your registered Email address')).done(() => {
                //       this._router.navigate(['account/login']);
                //});
                //this.notify.info(this.l('Email has been sent to your registered Email address'));
                //let msg = 'Email has been sent to your registered Email address';
                //let type = 'rose'
                //this.notify(msg, type);
                    //this._router.navigate(['account/login']);
               
                    this.saving = false;
            });

            console.log(this._router.navigate(['account/login']))


    }

    //notify(msg: any, type: any) {
    //    $.notify({
    //        icon: "add_alert",
    //        message: msg

    //    }, {
    //            type: type, // type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary']
    //            timer: 4000,
    //            placement: {
    //                from: 'bottom', // from = ['top','bottom']
    //                align: 'right' // align = ['left','center','right']
    //            }
    //        });
    //}

    }

