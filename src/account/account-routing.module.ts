import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AccountComponent } from './account.component';
import { ForgotPasswordComponent } from './password/forgot-password.component';
import { Register1Component } from './register1/register1.component';
import { UserregisterComponent } from './userregister/userregister.component';
import { ChangepasswordComponent } from '@app/account/changepassword/changepassword.component';


@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: AccountComponent,
                children: [
                    { path: 'login', component: LoginComponent },
                    { path: 'register', component: RegisterComponent },
                    { path: 'forgotpassword', component: ForgotPasswordComponent },
                    { path: 'register1', component: Register1Component },
                    { path: 'userregister', component: UserregisterComponent },
                    
                    { path: 'userregist', component: ChangepasswordComponent },
                    
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class AccountRoutingModule { }
