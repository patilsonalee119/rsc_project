import { Component, ViewContainerRef, OnInit, ViewEncapsulation, Injector } from '@angular/core';
import { LoginService } from './login/login.service';
import { AppComponentBase } from '@shared/app-component-base';

@Component({
    templateUrl: './account.component.html',
    styleUrls: [
        './account.component.less'
    ],
    encapsulation: ViewEncapsulation.None
})
export class AccountComponent extends AppComponentBase implements OnInit {

    versionText: string;
    currentYear: number;

    private viewContainerRef: ViewContainerRef;

    public constructor(
        injector: Injector,
        private _loginService: LoginService
    ) {
        super(injector);

        this.currentYear = new Date().getFullYear();
        this.versionText = this.appSession.application.version + ' [' + this.appSession.application.releaseDate.format('YYYYDDMM') + ']';
    }

    showTenantChange(): boolean {
        return abp.multiTenancy.isEnabled;
    }

    ngOnInit(): void {
        if(location.href.indexOf('account/login') > 0){
            $('body').attr('class', 'login-page');
           
            // this.isLoginPage = true;
            // this.isSettingPage = false;
        }else if(location.href.indexOf('account/forgotpassword') > 0){
            $('body').attr('class', 'password');
           
        }else if(location.href.indexOf('account/register') > 0){
            $('body').attr('class', 'register');
           
        }else if(location.href.indexOf('pages/resetPassword') > 0){
            $('body').attr('class', 'resetpwd');
           
        }
        else if(location.href.indexOf('account/Changepassword') > 0){
            $('body').attr('class', 'changepwd');
           
        }
        else if(location.href.indexOf('app/home') > 0){
            $('body').attr('class', 'home');
           
        }
        else if(location.href.indexOf('account/userregister') > 0){
            $('body').attr('class', 'userregister');
        }
        else{
            // $('body').attr('class', 'rscclass');
        }
    }
}

