import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientJsonpModule } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';

import { ModalModule } from 'ngx-bootstrap';

import { AbpModule } from '@abp/abp.module';

import { AccountRoutingModule } from './account-routing.module';

import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';

import { SharedModule } from '@shared/shared.module';

import { AccountComponent } from './account.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AccountLanguagesComponent } from './layout/account-languages.component';

import { LoginService } from './login/login.service';
import { ForgotPasswordComponent } from './password/forgot-password.component';

// tenants
import { TenantChangeComponent } from './tenant/tenant-change.component';
import { TenantChangeDialogComponent } from './tenant/tenant-change-dialog.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { Register1Component } from './register1/register1.component';
import { UserregisterComponent } from './userregister/userregister.component';
import { MyCommonModule } from '../app/Shared/common.module';
import { ChangepasswordComponent } from '@app/account/changepassword/changepassword.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpClientModule,
        HttpClientJsonpModule,
        AbpModule,
        SharedModule,
        ServiceProxyModule,
        AccountRoutingModule,
        ModalModule.forRoot(),
        MyCommonModule
    ],
    declarations: [
        AccountComponent,
        LoginComponent,
        RegisterComponent,
        AccountLanguagesComponent,
        // tenant
        ForgotPasswordComponent,
        TenantChangeComponent,
        ChangepasswordComponent,
        TenantChangeDialogComponent,
        
        ResetpasswordComponent,
        Register1Component,
        UserregisterComponent,
    ],
    providers: [
        LoginService
    ],
    entryComponents: [
        // tenant
        ChangepasswordComponent,
        TenantChangeDialogComponent,
        
        

    ]
})
export class AccountModule {

}
