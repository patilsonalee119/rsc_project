import { Component, Injector, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/app-component-base';
import { LoginService } from './login.service';
import { AbpSessionService } from 'abp-ng2-module/dist/src/session/abp-session.service';
import { IsTenantAvailableInput, AccountServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppTenantAvailabilityState } from '@shared/AppEnums';
import { accountModuleAnimation } from '@shared/animations/routerTransition';

// import 'rxjs/add/operator/finally';

@Component({
    templateUrl: './login.component.html',
    styleUrls: [
        './login.component.less'
    ],
    animations: [accountModuleAnimation()]
})
export class LoginComponent extends AppComponentBase {
  submitting: boolean = false;
    tenancyName: string = "Test";
    saving: boolean = false;

  constructor(
    injector: Injector,
    public loginService: LoginService,
    private _router: Router,
    private _sessionService: AbpSessionService,
    private _accountService: AccountServiceProxy
  ) {
    super(injector);
  }

  ngAfterViewInit(): void {
    // $(this.cardBody.nativeElement).find('input:first').focus();
}


get multiTenancySideIsTeanant(): boolean {
  return this._sessionService.tenantId > 0;
}


get isSelfRegistrationAllowed(): boolean {
  if (!this._sessionService.tenantId) {
      return false;
  }

  return true;
}

// forget(){
//   this._router.navigate(["/account/forgotpassword"]);
// }

getTenant(): void {
  //console.log(this.loginService.authenticateModel.userNameOrEmailAddress.split('@').pop().split('.').shift());
  if(this.loginService.authenticateModel.userNameOrEmailAddress !== undefined) {
      if (this.loginService.authenticateModel.userNameOrEmailAddress.indexOf('@') == -1)
          this.tenancyName = undefined;
      else
          this.tenancyName = this.loginService.authenticateModel.userNameOrEmailAddress.split('@').pop().split('.').shift();

      if (this.tenancyName === "828Logistics")
          this.tenancyName = undefined;

      if (!this.tenancyName) {
          abp.multiTenancy.setTenantIdCookie(undefined);
          return;
      }

      var input = new IsTenantAvailableInput();
      input.tenancyName = this.tenancyName;

      this.saving = true;
      this._accountService.isTenantAvailable(input)
          .subscribe((result) => {
              switch (result.state) {
                  case AppTenantAvailabilityState.Available:
                      abp.multiTenancy.setTenantIdCookie(result.tenantId);
                      return;
                  case AppTenantAvailabilityState.InActive:
                      this.message.warn(this.l('TenantIsNotActive', this.tenancyName));
                      break;
                  case AppTenantAvailabilityState.NotFound: //NotFound
                      //this.message.warn(this.l('ThereIsNoTenantDefinedWithName{0}', this.tenancyName));
                      break;
              }
              this.saving = false;
          });
  }
  
}

login(): void {
  this.submitting = true;
  this.loginService.authenticate(
      () => this.submitting = false
  );
}
}

