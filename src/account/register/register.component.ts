import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import {
  CompanyAppServicesServiceProxy,
  EmailSenderServiceProxy,
  AccountServiceProxy,
  RegisterInput,
  RegisterOutput
} from '@shared/service-proxies/service-proxies';
import { filter } from 'rxjs/operators';
import { AppConsts } from '@shared/AppConsts';

import { LoginService } from '../login/login.service';



import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef, OnInit, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { UserServiceProxy, CreateUserDto, RoleDto } from '@shared/service-proxies/service-proxies';
import { pipe } from '../../../node_modules/rxjs';
declare const $: any;

@Component({
  templateUrl: './register.component.html',
  animations: [accountModuleAnimation()],
  styles: [
    `
      mat-form-field {
        width: 100%;
      }
      mat-checkbox {
        padding-bottom: 5px;
      }
    `
  ]
})
export class RegisterComponent implements OnInit {
  model: RegisterInput = new RegisterInput();
  @ViewChild('modalContent', {static: false}) modalContent: ElementRef;


  saving: boolean = false;
  //user: CreateUserDto = null;
  user = new CreateUserDto();
  roles: RoleDto[] = null;
  path: any;
  Emaildata: any;
  IsActive: Boolean = true

  emailAddress: any
  name: any
  surname: any;
  password: any;
  CompanyName:any=[];
  constructor(
    injector: Injector,
    private _accountService: AccountServiceProxy,
    private _router: Router,
    private _loginService: LoginService,
    private _userService: UserServiceProxy,
    private emailSender: EmailSenderServiceProxy,
    private companyservice: CompanyAppServicesServiceProxy,


  ) {


  }

  ngOnInit() {
    this._userService.getRoles()
      .subscribe((result) => {
        this.roles = result.items;
      });
  }

  getAllCompany(){

    this.companyservice.getAll().subscribe(result=>{

      this.CompanyName=result;
    })
  }
  back(): void {
    this._router.navigate(['/login']);
  }
  uri: any
  save(): void {
    //TODO: Refactor this, don't use jQuery style code
    //var roles = ['Admin'];


    var roles = [];
    $(this.modalContent.nativeElement).find("[name=role]").each((ind:number, elem:Element) => {
      console.log(elem,"NAtiveelem")
        if($(elem).is(":checked") == true){
            roles.push(elem.getAttribute("value").valueOf());
        }
    });

    this.user.roleNames = roles;
    this.user.userName = this.user.emailAddress;
    this.user.isActive = true
    this.user.password = '';

    console.log(this.user);
    let UseMail = this.user
    this._accountService.sendPassworduser(this.user).subscribe((result) => {
      if (result == 1) {
        this.sendformail(UseMail)
      }
      else {
     //   alert("Already existed in System")
        this._accountService.checkUSER().subscribe((result)=>{
          
        })
        this.clear();

      }
    })
   
    // this._router.navigateByUrl('/app/home');


    //  var initialUrl =location.href;
    //  // let initialUrl = UrlHelper.initialUrl;
    //   if (initialUrl.indexOf('/account/register') > 0) {
    //       initialUrl = AppConsts.appBaseUrl + '/app/home';
    //   }

    //   location.href = initialUrl;

  }


  sendformail(UseMail) {
    let toEmail;
    let mailSubject;
    let mailMessage;

    toEmail = UseMail.emailAddress



    mailSubject = "Application User Password Link";
    let UserDetails = JSON.stringify(UseMail)
    let Encripted = btoa(UserDetails)
    console.log(Encripted, "UserDetails");
    // btoa(UserDetails)
    mailMessage = "Please click <a href='" + window.location.origin + "/pages/PasswordMail/" + Encripted + "' style = 'color : red'>HERE</a> to create Password for User."; //+ " EST";



    this.Emaildata = {
      toEmail: toEmail,
      mailSubject: mailSubject,
      mailMessage: mailMessage,
      //  carrierId: carrierID
    }
    this.emailSender.sendMail(this.Emaildata)
      .subscribe(res => {

        let msg = 'Sent Mail Successfully';
        let type = 'success';
        this.notify(msg,type);
        this._router.navigateByUrl('/account/login');
        this.clear();
      });

  }
 
  clear() {

    this.user.emailAddress = "";
    this.user.name = "";
    this.user.surname = "";
    this.user.name = "";
    this.user.site="";
    this.user.companyName="";
    this.user.totalAmount="";    
  }
  notify(msg: any, type: any) {
    $.notify({
      icon: "add_alert",
      message: msg

    }, {
      type: type, // type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary']
      timer: 4000,
      placement: {
        from: 'bottom', // from = ['top','bottom']
        align: 'right' // align = ['left','center','right']
      }
    });
  }
}
