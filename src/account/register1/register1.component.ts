import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
declare const $: any;

import {
  EmailSenderServiceProxy,
  AccountServiceProxy,
  RegisterInput,
  RegisterOutput, UserServiceProxy, CreateUserDto, RoleDto
} from '@shared/service-proxies/service-proxies';
import { LoginService } from 'account/login/login.service';
@Component({
  selector: 'app-register1',
  templateUrl: './register1.component.html',
  styleUrls: ['./register1.component.css']
})
export class Register1Component implements OnInit {

  @ViewChild('modalContent', {static: false}) modalContent: ElementRef;

  model: RegisterInput = new RegisterInput();


  saving: boolean = false;
  //user: CreateUserDto = null;
  user = new CreateUserDto();
  roles: RoleDto[] = null;
  path: any;
  Emaildata: any;
  IsActive: Boolean = true

  emailAddress: any
  name: any
  surname: any;
  password: any;

  constructor(
    private _accountService: AccountServiceProxy,
    private _router: Router,
    private _loginService: LoginService,
    private _userService: UserServiceProxy,
    private emailSender: EmailSenderServiceProxy,

  ) {


  }

  ngOnInit() {
    this._userService.getRoles()
      .subscribe((result) => {
        this.roles = result.items;
      });
  }

  back(): void {
    this._router.navigate(['/login']);
  }
  uri: any
  save(): void {
    //TODO: Refactor this, don't use jQuery style code
    //var roles = ['Admin'];

    var roles = [];
    $(this.modalContent.nativeElement).find("[name=role]").each((ind:number, elem:Element) => {
        if($(elem).is(":checked") == true){
            roles.push(elem.getAttribute("value").valueOf());
        }
    });

    this.user.roleNames = roles;
    this.user.userName = this.user.emailAddress;
    this.user.isActive = true
    
    this._userService.create(this.user)
.subscribe((res) => {

  let msg = 'Saved Successfully';
  let type = 'success';
  this.notify(msg,type);
  this.clear();
});


   
     this._router.navigateByUrl('/account/login');


    //  var initialUrl =location.href;
    //  // let initialUrl = UrlHelper.initialUrl;
    //   if (initialUrl.indexOf('/account/register') > 0) {
    //       initialUrl = AppConsts.appBaseUrl + '/app/home';
    //   }

    //   location.href = initialUrl;

  }


  clear() {

    this.user.emailAddress = "";
    this.user.name = "";
    this.user.surname = "";
    this.user.password = "";
    this.user.name = "";
    this.user.site="";
    this.user.companyName="";
    this.user.confirm_password="";
  }

  notify(msg: any, type: any) {
    $.notify({
      icon: "add_alert",
      message: msg

    }, {
      type: type, // type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary']
      timer: 1000,
      placement: {
        from: 'bottom', // from = ['top','bottom']
        align: 'right' // align = ['left','center','right']
      }
    });
  }
}